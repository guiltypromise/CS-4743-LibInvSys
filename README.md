# CS 4743 Library Inventory System

##### Tasks:
 - [X] 30 pts Single composite view for both Library and Library Book data
 - [X] 20 pts Library CRUD works correctly
 - [X] 20 pts Library Book CRUD works correctly
 - [X] 10 pts Audit trail correctly implemented for Library and Library Books
 - [X] 20 pts Program generates a PDF report that contains correct Library and Library Book data with page breaks
 - [X] 10 pts Implement library book deletion that deletes the library book from the database. Please include a comment in your submission if you implement this form of deletion.

###### Notes:
VM Arguments:
-Dlog4j.configurationFile=log4j2.xml
https://easel2.fulgentcorp.com/phpmyadmin/
MIsYyxgDYxBayDz48BEX