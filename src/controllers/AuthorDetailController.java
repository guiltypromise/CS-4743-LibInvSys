/**
 * AuthorDetailController.java 
 * @author Jason Martin
 * 
 * Controls the backend logic for the Author Detail View provided
 * by the authordetailview.fxml
 * 
 * http://stackoverflow.com/questions/30814258/javafx-pass-parameters-while-instantiating-controller-class
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import models.Author;

public class AuthorDetailController implements Initializable 
{
	private static final Logger logger = LogManager.getLogger(AuthorDetailController.class);

	@FXML private AnchorPane rootAnchorPane;
	@FXML private Label titleLabel;
	@FXML private Label errorLabel;
	@FXML private TextField firstName;
	@FXML private TextField lastName;
	@FXML private TextField userWebsite;
	@FXML private ComboBox<String> genderSelect;
	@FXML private DatePicker dobSelect;
	@FXML private Button auditViewButton;
	@FXML private Button saveButton;
	@FXML private Button backButton;
	@FXML private Button refreshButton;
	private Author author; //reference to an Author template
	private int savedSessionId;
	
	//make constructor here with reference to an author

	public AuthorDetailController(Author author, int sessionId) {
		this.author = author;
		this.savedSessionId = sessionId;
	}
	
	@FXML private void handleRefreshAction( ActionEvent event ) throws IOException
	{
		logger.info("Refresh button pressed");

		try {
			Author selected = MasterController.getInstance().getAuthorGateway().getAuthorById( author.getId() );
			MasterController.getInstance().changeView(ViewType.AUTHOR_DETAIL, selected, savedSessionId );
		} catch (SQLException e) {
			logger.error("Refreshing operation failed");
			//e.printStackTrace();
		}
	}
	
	@FXML private void handleAuditViewAction( ActionEvent event ) throws IOException
	{
		try{
			logger.info("Audit Trail button pressed");
				
			// this is for when we just added and saved a new Author and want to 
			// view his Audit Trail immediately
			//if( MasterController.getInstance().getLastSaved() > 0 )
			//	author.setId( MasterController.getInstance().getLastSaved());
			
			// set the proxy author model values from the GUI fields to pass for saving if user wanted to commit
			//MasterController.getInstance().setAuthorToSave( returnAuthorToSave() );

			// call master controller singleton
			// for changing the view from this button to author's audit trail
			MasterController.getInstance().changeView(ViewType.AUDIT_VIEW, author, savedSessionId );	
			
		} catch (Exception e ) {
			logger.error("IO Detail Audit Button error: " + e.getMessage());
		}
	}

	@FXML private void handleSaveAction( ActionEvent event ) throws IOException 
	{
		//Object source = event.getSource();
		
		try {
			logger.info("SAVE Button pressed");
			
			// prevent inserting duplicates and perform update instead
			// by getting state information from MasterController singleton
			if( MasterController.getInstance().getLastSaved() > 0 )
			{
				author.setId( MasterController.getInstance().getLastSaved());
				//MasterController.getInstance().setLastSaved( 0 );
			}	
			String dob = null;
			if( dobSelect.getValue() == null )
				logger.error("No dob selected");
			else
				dob = dobSelect.getValue().toString();
			
				author.saveAuthor( author.getId() 
					, firstName.getText()
					, lastName.getText()
					, dob
					, genderSelect.getValue()
					, userWebsite.getText() );
			if( author.getId() < 1 )
				author.setId(MasterController.getInstance().getLastSaved());
			
			logger.info(" Saving " + author.getId() + " as ID");
			this.author.setLastModified( MasterController.getInstance().getAuthorGateway().getAuthorById( author.getId() ).getLastModified());
			logger.info(" Last Saved: " + author.getLastModified() );

			MasterController.getInstance().saveConfirmedDialog();
			
			// reset edited state since we saved here
			MasterController.getInstance().setEditedState( false, 0 ); 
			
		} catch( Exception e ) {
			//e.printStackTrace();
			MasterController.getInstance().errorConfirmedDialog( e.getMessage());
			logger.error("IO Detail Save Button error: " + e.getMessage());
		}
	}
	
	@FXML private void handleBackAction( ActionEvent event ) throws IOException 
	{
		try {
			logger.info("Back Button pressed");
			
			if( MasterController.getInstance().getLastSaved() > 0 )
				author.setId( MasterController.getInstance().getLastSaved());
			
			// set the proxy author model values from the GUI fields to pass for saving if user wanted to commit
			//MasterController.getInstance().setAuthorToSave( returnAuthorToSave() );
			
			MasterController.getInstance().changeView(ViewType.AUTHOR_LIST, author, savedSessionId );		
		} catch(Exception e ) {
			logger.error("IO Detail Back Button error: " + e.getMessage());
		}
	}

	/**
	 * Initialize and populate the Author Detail panel with data.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources ) 
	{
		logger.info(String.format(" Init Author Details" ));
		titleLabel.setText("Add Author");

		// populate GUI fields here
		firstName.setText( author.getFirstName());
		// somehow do a changelistener here?
		
		
		lastName.setText( author.getLastName());

		// Use proxy data as a model for the combo box.
		ObservableList<String> genders = FXCollections.observableArrayList(
	          "M", "F");
		if( author.getId() > 0  )
		{
			titleLabel.setText("Edit Author Details");
			dobSelect.setValue( author.getDateOfBirth() );
			genderSelect.setValue( author.getGender() );
			logger.info(String.format(" %s last_modified %s"
					, author.getId()
					, author.getLastModified()));
			auditViewButton.setVisible( true );
			refreshButton.setVisible( true );
		}
		
		genderSelect.getItems().addAll( genders );
		genderSelect.setPromptText( "Select" );
		userWebsite.setText( author.getWebsite() );
		
		// set listeners on the GUI fields
		// might have to refactor this later
		
		firstName.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, author.getId() );
				author.setFirstName( newValue );
				MasterController.getInstance().setAuthorToSave( author );

			}
		});
		
		lastName.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, author.getId() );
				author.setLastName( newValue );
				MasterController.getInstance().setAuthorToSave( author );
			}
		});
		
		genderSelect.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() 
		{
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, author.getId() );
				author.setGender( newValue );
				MasterController.getInstance().setAuthorToSave( author );
			}
		});
		
		dobSelect.valueProperty().addListener( new ChangeListener<LocalDate>() 
		{

			@Override
			public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue,
					LocalDate newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, author.getId() );
				author.setDateOfBirth( newValue.toString() );
				MasterController.getInstance().setAuthorToSave( author );
			}
		});
		
		userWebsite.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, author.getId() );
				author.setWebsite( newValue );
				MasterController.getInstance().setAuthorToSave( author );
			}
		});
		
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			saveButton.setDisable( true);
		}
		// else check the role
		// if session is admin
		if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL)
				||
			MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL))
		{
			saveButton.setDisable( false );
		}
		else // intern or guest
		{
			saveButton.setDisable( true);
		}
	}
}
