/**
 * MenuController.java
 * @author Jason Martin
 * 
 * Contains backend logic that translates Scene logic selections
 * from FXML Views.
 */

package controllers;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.util.Pair;
import models.Author;
import models.Book;
import models.Library;


public class MenuController implements Initializable 
{
	// Logging manager object for this class
	private static final Logger logger = LogManager.getLogger(MenuController.class);

	@FXML private MenuBar menuBar; // the menuBar from the FXML
	
	/* Menu Items */
	@FXML private MenuItem menuItemExit; 
	@FXML private MenuItem menuItemHome; 
	@FXML private MenuItem menuItemShowAuthors; 
	@FXML private MenuItem menuItemAddAuthor;
	@FXML private MenuItem menuItemRemoveAuthor;
	@FXML private MenuItem menuItemShowBooks;
	@FXML private MenuItem menuItemAddBook;
	@FXML private MenuItem menuItemShowLibraries;
	@FXML private MenuItem menuItemAddLibrary;
	@FXML private MenuItem menuItemLogin;
	@FXML private MenuItem menuItemLogout;
	@FXML private Menu menuQuitButton;

	private int sessionId;

	
	public MenuController()
	{
		super();
		sessionId = AuthenticatorConstants.INVALID_SESSION;

	}

	/**
	 * Handles menu item actions the user selects from the menu.
	 * @param event The particular event selected.
	 * @throws IOException Throw an error if item can't be found matching logic.
	 */
	@SuppressWarnings("static-access")
	@FXML private void handleMenuAction(ActionEvent event ) throws IOException 
	{
		Object source = event.getSource();
		Object data = null;

		try {	
			// Have either an Author or a Book with possible unsaved changes when
			// switching views
			if( MasterController.getInstance().getEditedState() == true )
			{
				// check if we have existing last saved data from MasterController
				if( !(MasterController.getInstance().getAuthorToSave() == null) )
				{
					data = MasterController.getInstance().getAuthorToSave();
				}
				if( !(MasterController.getInstance().getBookToSave() == null) )
				{
					data = MasterController.getInstance().getBookToSave();
				}
			}
			
			// If the Exit menu item is selected
			if( source == menuItemExit ) {
				logger.info("Closing application");
				doLogout();
				Platform.exit();
			}
			else if (source == menuItemShowAuthors  ) {
				logger.info("Selected 'Show Authors List' view");
				MasterController.getInstance().changeView(ViewType.AUTHOR_LIST
							//, MasterController.getInstance().getAuthorToSave());
							, data
							, sessionId );
			}
			else if ( source == menuItemAddAuthor ){
				logger.info("Selected 'Add Author' view");
				MasterController.getInstance().changeView(ViewType.AUTHOR_DETAIL
							, new Author()
							, sessionId);
			}
			else if (source == menuItemShowBooks ) {
				logger.info("Selected 'Show Books List' view");
				MasterController.getInstance().changeView(ViewType.BOOK_LIST
							, data 
							, sessionId );
			}
			else if (source == menuItemAddBook) {
				logger.info("Selected 'Add Book' view");
				MasterController.getInstance().changeView(ViewType.BOOK_DETAIL
							, new Book(), sessionId );
			}
			else if (source == menuItemShowLibraries ) {
				logger.info("Selected 'Show Libraries List' view");
				MasterController.getInstance().changeView(ViewType.LIBRARY_LIST
							, data, sessionId );
			}
			else if (source == menuItemAddLibrary) {
				logger.info("Selected 'Add Library' view");
				MasterController.getInstance().changeView(ViewType.LIBRARY_DETAIL
							, new Library(), sessionId);
			}
			else if ( source == menuItemHome ) {
				logger.info("Selected 'Home' view");
				MasterController.getInstance().changeView(ViewType.HOME_VIEW
							//, MasterController.getInstance().getAuthorToSave());
							, data, sessionId );
			} else if ( source == menuItemLogin ){
				logger.info("Selected 'Login' modal: " 
			        + MasterController.getInstance().getAuthBean().test());
				doLogin(MasterController.getInstance().getAuthBean());
				MasterController.getInstance().changeView(ViewType.HOME_VIEW
						//, MasterController.getInstance().getAuthorToSave());
						, data, sessionId );

			} else if ( source == menuItemLogout ){
				logger.info("Selected 'Logout' ");
				doLogout();
				MasterController.getInstance().changeView(ViewType.HOME_VIEW
						//, MasterController.getInstance().getAuthorToSave());
						, data, sessionId );
				MasterController.getInstance().setEditedState(false, 0);
				MasterController.getInstance().setLastSaved(0);
			} else {
				logger.warn("Unhandled menu action, defaulting to Home view");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("IO Menu Error: " + e.getMessage() );
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		menuBar.setFocusTraversable( true );
		updateMenuAccess();
	}
	
	@SuppressWarnings("static-access")
	private void doLogin( AuthEJBRemote authBean )
	{
		Pair<String, String> creds = MasterController.getInstance().loginDialog();
		if(creds == null) //canceled
			return;
		
		String userName = creds.getKey();
		String pw = creds.getValue();
		
		logger.info("userName is " + userName + ", password is " + pw);
		
		//hash password
		String pwHash = CryptoStuff.sha256(pw);
		
		logger.info("sha256 hash of password is " + pwHash);
		
		//send login and hashed pw to authenticator
		try {
			//if get session id back, then replace current session
			this.sessionId = authBean.loginSha256(userName, pwHash);
			MasterController.getInstance().setSession(sessionId);
			
			logger.info("session id is " + sessionId);
			
		} catch (Exception e) {
			//else display login failure
			Alert alert = new Alert(AlertType.WARNING);
			alert.getButtonTypes().clear();
			ButtonType buttonTypeOne = new ButtonType("OK");
			alert.getButtonTypes().setAll(buttonTypeOne);
			alert.setTitle("Login Failed");
			alert.setHeaderText("The user name and password you provided do not match stored credentials.");
			alert.showAndWait();

			return;
		}
		
		//restrict access to GUI controls based on current login session
		updateMenuAccess();
		
	}
	
	private void doLogout(){
		
		MasterController.getInstance().getAuthBean().logout(sessionId);
		sessionId = AuthenticatorConstants.INVALID_SESSION;
		MasterController.getInstance().setSession(sessionId);
		//restrict access to GUI controls based on current login session
		updateMenuAccess();
	}
	
	private void updateMenuAccess()
	{
		
		//if logged in, login should be disabled
		if(sessionId == AuthenticatorConstants.INVALID_SESSION)
		{
			menuItemLogin.setDisable(false);
		}
		else
		{
			menuItemLogin.setDisable(true);
		}
		
		//if not logged in, logout should be disabled
		if(sessionId == AuthenticatorConstants.INVALID_SESSION)
		{
			menuItemLogout.setDisable(true);
			menuItemAddBook.setDisable(true);
			menuItemAddAuthor.setDisable(true);
			menuItemAddLibrary.setDisable(true);
			
			menuItemShowAuthors.setDisable(true);
			menuItemShowBooks.setDisable(true);
			menuItemShowLibraries.setDisable(true);
		}
		else
		{
			menuItemLogout.setDisable(false);
			menuItemAddBook.setDisable(false);
			menuItemAddAuthor.setDisable(false);
			menuItemAddLibrary.setDisable(false);
			menuItemShowAuthors.setDisable(false);
			menuItemShowBooks.setDisable(false);
			menuItemShowLibraries.setDisable(false);
		}
		
		// if session is admin
//		if( MasterController.getInstance().getAuthBean().hasRole(sessionId).equals(AuthenticatorConstants.ADMIN_LEVEL) 
//					||
//			MasterController.getInstance().getAuthBean().hasRole(sessionId).equals(AuthenticatorConstants.LIBRARIAN_LEVEL) 
//			)
			
		if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL)
				||
			MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL))
		{
			menuItemAddBook.setDisable(false);
			menuItemAddAuthor.setDisable(false);
			menuItemAddLibrary.setDisable(false);
		}
		else // intern or guest
		{
			menuItemAddBook.setDisable(true);
			menuItemAddAuthor.setDisable(true);
			menuItemAddLibrary.setDisable(true);
		}
		
		//update fxml labels
		//textLogin.setText(auth.getUserNameFromSessionId(sessionId));
		//textSessionId.setText("Session id " + sessionId);
		
		//update menu info based on current user's access privileges
//		if(auth.hasAccess(sessionId, AuthenticatorConstants.CAN_ACCESS_CHOICE_1))
//			choice1.setDisable(false);
//		else 
//			choice1.setDisable(true);
//		if(auth.hasAccess(sessionId, AuthenticatorConstants.CAN_ACCESS_CHOICE_2))
//			choice2.setDisable(false);
//		else 
//			choice2.setDisable(true);
//		if(auth.hasAccess(sessionId, AuthenticatorConstants.CAN_ACCESS_CHOICE_3))
//			choice3.setDisable(false);
//		else 
//			choice3.setDisable(true);
		
	}
	
} // end MenuController 
