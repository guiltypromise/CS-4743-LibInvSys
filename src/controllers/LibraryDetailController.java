/**
� edit a new or existing library�s name
� add a new book to the library�s inventory
� change a quantity associated with a book in the library�s inventory
� delete a library book from the inventory (or zero out the quantity)
� save all changes to the library data or its library book data
� generate a PDF inventory report (see task #10)
� transition to the library�s audit trail screen
 */

package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Alert.AlertType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import models.Author;
import models.Book;
import models.Library;
import models.LibraryBook;

public class LibraryDetailController implements Initializable
{
	private static final Logger logger = LogManager.getLogger(LibraryDetailController.class);
	//private static final String DEST = "InvReport.pdf";

	private List<LibraryBook> libBooks;
	private Library library;
	private LibraryBook selectedLibBook;
	private List<LibraryBook> pendingDeleteBooks;
	// Init the ComboBox of Books 	
	private ComboBox<Book> bookSelect = new ComboBox<Book>();
	private ObservableList<Book> booksList;
	ObservableList<LibraryBook> bookInvElements;
	
	@FXML private ListView<LibraryBook> libraryBookListView;
	@FXML private TextField libraryName;
	@FXML private Button buttonSave;
	@FXML private Button buttonRefresh;
	@FXML private Button buttonInvReport;
	@FXML private Button buttonAuditTrail;
	@FXML private Button buttonBack;
	@FXML private Button buttonDeleteBook;
	@FXML private Button buttonAddBook;
	@FXML private Label libraryDetailLabel;
	private int savedSessionId;
	
	public LibraryDetailController(Library library, List<LibraryBook> libraryBooks, int sessionId) 
	{
		this.library = library;
		this.libBooks = libraryBooks;
		this.bookSelect = new ComboBox();
		this.booksList = bookSelect.getItems();
		this.pendingDeleteBooks = new ArrayList<LibraryBook>();
		savedSessionId = sessionId;

	}
	
	@FXML private void handleInvReportAction(ActionEvent event) throws IOException, FileNotFoundException, DocumentException
	{
		logger.info("Inv Report button pressed");
		
		// use String Builder 
		
//		Library: Library Name (in a report header)
//			for each book record:
//				Book Title (in a tabular/detail section of the report)
//				Book�s Author�s Full Name (in a tabular/detail section of the report)
//				Book Publisher (in a tabular/detail section of the report)
//				Quantity of Book in that Library (in a tabular/detail section of the report)
		
		//create a new PDF document and attach it to the file we have created
		Document document = new Document();
		String filename = String.format("%s_Summary_%s.pdf", library.getLibraryName().replace(' ', '_'), LocalDate.now());
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();
        //display title background rectangle
        PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(36, document.getPageSize().getHeight() - 36
        		, document.getPageSize().getWidth() - 36, document.getPageSize().getHeight() - 76);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(2);
        rect.setBackgroundColor(BaseColor.LIGHT_GRAY);
        canvas.rectangle(rect);
        
        //overlay title text on top of rectangle
        ColumnText ct = new ColumnText(canvas);
        Rectangle rect2 = new Rectangle(36, document.getPageSize().getHeight() - 29
        		, document.getPageSize().getWidth() - 36, 40);
        ct.setSimpleColumn(rect2);
        Font f = new Font(FontFamily.HELVETICA, 25.0f, Font.BOLD, BaseColor.BLACK);
        Chunk c = new Chunk(String.format(" Library: %s", library.getLibraryName()), f);
        ct.addElement(new Paragraph(c));
        ct.go();
        
        document.add(new Paragraph("\n\n\n\n\n"));
        
        // create table with 3 evenly-spaced columns
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(100);
        String[] strColHeader = {"Book Title", "Author", "Publisher", "Quantity"}; 
        for( int i = 0; i < 4; i++ )
        {
        	PdfPCell cell = new PdfPCell( new Paragraph(strColHeader[i]));
        	cell.setPadding( 15 );
        	table.addCell(cell);
        }
        table.completeRow();
        for(int i = 0; i < library.getLibraryBooks().size(); i++)
        {
        	PdfPCell cellBookTitle = formatCell( library.getLibraryBooks().get(i).getBook().getTitle());
        	table.addCell( cellBookTitle );
        	
        	PdfPCell cellBookAuthor = formatCell( String.format ("%s %s"
            		, library.getLibraryBooks().get(i).getBook().getAuthor().getFirstName()
            		, library.getLibraryBooks().get(i).getBook().getAuthor().getLastName()));
        	table.addCell(cellBookAuthor);
        	
        	PdfPCell cellBookPublisher = formatCell(library.getLibraryBooks().get(i).getBook().getPublisher());
            table.addCell(cellBookPublisher);
            
            PdfPCell cellBookQuantity = formatCell(String.format("%s", library.getLibraryBooks().get(i).getQuantity()));
        	table.addCell(cellBookQuantity);

            table.completeRow();
        }
        document.add(table);
        
        document.close();
        
        // give confirmation dialog
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inventory Report Confirmation");
		alert.setHeaderText(String.format("Created \"%s\""
				, filename ));
		alert.setContentText(" The inventory report was saved to the project's top-level directory.");
		ButtonType buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE );
		
		alert.getButtonTypes().setAll(buttonTypeOK);
		
		//logger.info("DELETE button: waiting for confirmation...");

		Optional<ButtonType> result = alert.showAndWait();
	}
	
	@FXML private void handleSaveAction( ActionEvent event ) throws IOException 
	{
		try {
			logger.info("Save button pressed");
			// prevent inserting duplicates and perform update instead
			// by getting state information from MasterController singleton
			if( MasterController.getInstance().getLastSaved() > 0 )
			{
				library.setId( MasterController.getInstance().getLastSaved());
				
				for( LibraryBook book : libBooks )
				{
					logger.info(String.format("Checking  >> Library_Book [%s] last_modified: %s"
							, book.getBook().getId()
							, book.getLastModified()
							));
				}
			}
			
			library.saveLibrary( library.getId(), libraryName.getText(), libBooks);
			
			// do we have pending deletions after successful save?
			if( pendingDeleteBooks.size() > 0 )
			{
				MasterController.getInstance().getLibraryBookTableGateway().deleteLibraryBooks(library.getId()
						, pendingDeleteBooks );
			}
			
			if( library.getId() < 1 )
				library.setId(MasterController.getInstance().getLastSaved());
			
			logger.info(" Saving " + library.getId() + " as ID");
			this.library.setLastModified( MasterController.getInstance().getLibraryGateway().getLibraryById( library.getId() ).getLastModified());
			logger.info(" Last Saved: " + library.getLastModified() );
			
			MasterController.getInstance().saveConfirmedDialog();
			// reset edited state since we saved here
			MasterController.getInstance().setEditedState( false, 0 ); 
			
			// then refresh to see the most recent list 
			//Library selected = MasterController.getInstance().getLibraryGateway().getLibraryById( library.getId() );
			//MasterController.getInstance().changeView(ViewType.LIBRARY_DETAIL, selected );
		} catch ( Exception e ) {
			//e.printStackTrace();
			MasterController.getInstance().errorConfirmedDialog( e.getMessage());
			logger.error("IO Detail Save Button error: " + e.getMessage());
		}

	}
	
	@FXML private void handleAuditViewAction( ActionEvent event ) throws IOException
	{
		try{
			logger.info("Audit Trail button pressed");
			MasterController.getInstance().changeView(ViewType.AUDIT_VIEW, library, savedSessionId );	
			
		} catch (Exception e ) {
			e.printStackTrace();
			logger.error("IO Detail Audit Button error: " + e.getMessage());
		}
	}
	
	@FXML private void handleRefreshAction( ActionEvent event ) throws IOException
	{
		logger.info("Refresh button pressed");

		try {
			Library selected = MasterController.getInstance().getLibraryGateway().getLibraryById( library.getId() );
			MasterController.getInstance().changeView(ViewType.LIBRARY_DETAIL, selected, savedSessionId );
		} catch (SQLException e) {
			logger.error("Refreshing operation failed");
			//e.printStackTrace();
		}
	}
	
	@FXML private void handleBackAction( ActionEvent event ) throws IOException 
	{
		try {
			logger.info("Back Button pressed");
			
			if( MasterController.getInstance().getLastSaved() > 0 )
				library.setId( MasterController.getInstance().getLastSaved());
			
			// set the proxy author model values from the GUI fields to pass for saving if user wanted to commit
			//MasterController.getInstance().setAuthorToSave( returnAuthorToSave() );
			
			MasterController.getInstance().changeView(ViewType.LIBRARY_LIST, library, savedSessionId );		
		} catch(Exception e ) {
			logger.error("IO Detail Back Button error: " + e.getMessage());
		}
	}
	
	@FXML private void handleAddBookAction( ActionEvent event ) throws IOException 
	{
		logger.info("Add Book Button pressed");
		try {
			addBookActionDialog();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@FXML private void handleDeleteAction( ActionEvent event ) throws IOException
	{
		if( selectedLibBook != null )
		{
			logger.info("Delete Book for \'" + selectedLibBook.getBook().getTitle() + "\' pressed");
			
			deleteBookActionDialog();

		}
		else
		{
			logger.error("No LibraryBook selected for Delete");
			MasterController.getInstance().errorConfirmedDialog("Select a book record from the inventory list for deletion.");

		}
	}
	 
	@FXML private void handleBookListAction( MouseEvent event ) throws IOException
	{
		try
		{
		this.selectedLibBook = libraryBookListView.getSelectionModel().getSelectedItem();
		
		logger.info("Selected \'" + this.selectedLibBook.getBook().getTitle() + "\' pressed");
		}
		catch( Exception e)
		{
			logger.error("Unhandled book inventory list error");
		}


	}
	
	private void deleteBookActionDialog() 
	{
		try 
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Delete Book");
			alert.setHeaderText(String.format("\'%s\' will be pending for deletion."
					, this.selectedLibBook.getBook().getTitle() ));
			alert.setContentText("The library book record will be removed from this Library's inventory after changes are saved.");
			ButtonType buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE );
			
			alert.getButtonTypes().setAll(buttonTypeOK);
			
			logger.info("DELETE button: waiting for confirmation...");
	
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOK)
			{
				MasterController.getInstance().setEditedState( true, library.getId() );
				// remove from the libBooks list
				logger.info("Selected delete last modified: " + selectedLibBook.getLastModified());
				libBooks.remove(selectedLibBook);
				library.setLibraryBooks((ArrayList<LibraryBook>) libBooks);
				pendingDeleteBooks.add(selectedLibBook);
				//pendingDeleteBooks.get(pendingDeleteBooks.size()-1).setLastModified(selectedLibBook.getLastModified());;
				//logger.info("Pend Delete Last_modified: " + pendingDeleteBooks.get(pendingDeleteBooks.size()-1).getLastModified());
	
				MasterController.getInstance().setLibraryToSave( library, libBooks, pendingDeleteBooks );
				bookInvElements.clear();
				bookInvElements.addAll(libBooks);
	
				// add to pendingDelete list
				// show 
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void addBookActionDialog() throws Exception
	{	
		// display modal dialog box for confirmation
		try {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Add/Edit Book");
			alert.setHeaderText("Please select a Book to add/edit and specify a quantity >= 0");
			alert.setContentText("Note: Choosing a book that exists in the inventory already can be modified here.");
			
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(20, 150, 10, 10));
	
			Spinner<Integer> quantitySelect = new Spinner<Integer>();
			quantitySelect.setEditable(true);
			int initialVal = 1;
			// Value factory
			SpinnerValueFactory<Integer> valFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, initialVal);
			quantitySelect.setValueFactory(valFactory);
			
			bookSelect.setPromptText( "Select Book" );
	
			//PasswordField password = new PasswordField();
			//password.setPromptText("Password");
	
			grid.add(new Label("Book:"), 0, 0);
			grid.add( this.bookSelect, 1, 0);
			grid.add(new Label("Quantity"), 0, 1);
			grid.add(quantitySelect, 1, 1);
			alert.getDialogPane().setContent(grid);
			
			ButtonType buttonTypeOne = new ButtonType("Add/Edit");
			ButtonType buttonTypeTwo = new ButtonType("Cancel");
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
	
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne)
			{
				LibraryBook libBook = null;
				// retrieve the library book record from database
				Book selected = bookSelect.getSelectionModel().getSelectedItem();
				
				// is it an Existing book record?
				// if we do it this way, it will always pull the fresh record
				// which will get in our way of keeping stale records for optimistic locking
				libBook = MasterController.getInstance().getLibraryBookTableGateway().getLibraryBookByBookId( this.library.getId(), selected.getId());
				if( libBook == null)
				{
					logger.info("Preparing for new library book record");
					// make a new library book record
					libBook = new LibraryBook();
					
					// set lib book fields
					libBook.setBook(selected);
				}
				else
				{
					libBook.setQuantity(quantitySelect.getValue());
					logger.info("UPDATE EXISTING BOOK");
					logger.info(String.format("LibraryBook from DB: \'%s\' Qty: %s,"
							, libBook.getBook().getTitle(), libBook.getQuantity()));
					bookInvElements.clear();
					logger.info(String.format("Library_Book [%s] last_modified: %s"
							, libBook.getBook().getId()
							, libBook.getLastModified()
							));
					//ArrayList<LibraryBook> booksToRemove = new ArrayList<LibraryBook>();
					//for( LibraryBook entry : libBooks )
					for(int i = 0; i<libBooks.size(); i++ )
					{
						if( libBooks.get(i).getBook().getId() == libBook.getBook().getId() )
						{
							// hack to change back last_modified of pulled fresh one
							//libBook.setLastModified(libBooks.get(i).getLastModified() );
							logger.info(String.format("LibBook[%s]: \'%s\' Qty: %s"
									, i, libBooks.get(i).getBook().getTitle()
									, libBooks.get(i).getQuantity()));
		

							//libBooks.remove(entry);
							// if quantities are different
							if( libBooks.get(i).getQuantity() != libBook.getQuantity() )
							{
								logger.info("Value changed from " +libBooks.get(i).getQuantity() + " to " + libBook.getQuantity() );	
								MasterController.getInstance().setEditedState( true, library.getId() );
								libBooks.remove(i); // erase the old library book record
								//bookInvElements.remove(libBooks.contains(libB));
								//logger.info("FOUND MATCHING BOOK");
								//libBook.setQuantity(quantitySelect.getValue()); // update the quantity of existing libBook record
								libBooks.add(libBook); // add back in the new library book record
								library.setLibraryBooks((ArrayList<LibraryBook>) libBooks);
								MasterController.getInstance().setLibraryToSave( library, libBooks, pendingDeleteBooks );
							}
							else
							{
								libBooks.add(libBooks.get(i)); // add back in the new library book record
							}

							//libBook.setQuantity(quantitySelect.getValue()); // update the quantity of existing libBook record
							//bookInvElements.add(libBook); // add it the list view
							logger.info(String.format("Library_Book in libBooks [%s] last_modified: %s"
									, libBooks.get(i).getBook().getId()
									, libBooks.get(i).getLastModified()
									));
						}
						//else
						//{
						//	bookInvElements.add(libBooks.get(i));	
						//}
						
						if( i > 10000 )
						{
							throw new Exception("Library Record inventory error");
						}
					}
					
					bookInvElements.addAll(libBooks);
				}

				//bookInvElements = libraryBookListView.getItems();

				// update the existing record in the list
				if( libBook.isNewRecord() )
				{
					logger.info("Adding new Library Book: + " + libBook.getBook().getTitle() );	

					MasterController.getInstance().setEditedState( true, library.getId() );

					// libBook list has definitely been edited here
					//logger.info("INSERTING NEW BOOK");

					libBook.setQuantity(quantitySelect.getValue());
					//library.getLibraryBooks().add(libBook);
					libBooks.add(libBook);
					library.setLibraryBooks((ArrayList<LibraryBook>) libBooks);
					MasterController.getInstance().setLibraryToSave( library, libBooks, pendingDeleteBooks );
					bookInvElements.add( libBook );
				}
			}
		} 
		catch ( Exception e )
		{
			//e.printStackTrace();
			logger.error("Failed to add/edit library_book to Library book list");
			throw new Exception("Invalid input");

		}
	}
	
	public List<LibraryBook> getPendingDeleteBooks()
	{
		return pendingDeleteBooks;
	}

	private PdfPCell formatCell( String info )
	{
		PdfPCell cell = new PdfPCell( new Paragraph(info) );
		cell.setPaddingTop( 50 );
    	cell.setPaddingBottom( 50 );
    	cell.setPaddingLeft( 10 );
    	cell.setPaddingRight( 10 );
		return cell;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		logger.info(String.format(" Init Library Details" ));
		
		libraryDetailLabel.setText("Add Library");
		
		if( library.getId() > 0 )
		{
			libraryDetailLabel.setText("View and Edit Library Details");
			buttonAuditTrail.setVisible( true );
			buttonInvReport.setVisible( true );
			buttonRefresh.setVisible( true );
		}
		
		libraryName.setText(library.getLibraryName());
		
		bookInvElements = libraryBookListView.getItems();
		for(LibraryBook libraryBook : libBooks )
		{
			logger.info(
					String.format("Adding LibBook_id[%s] last_ modified: %s"
							, libraryBook.getBook().getId()
							, libraryBook.getLastModified())
					);
			bookInvElements.add(libraryBook);
		}	
		
		ArrayList<Book> books = null;

		try {
			books = (ArrayList<Book>) MasterController.getInstance().getBookGateway().getBooks();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// populate combo box for add book dialog
		for(Book book: books)
		{
			booksList.add(book);
		}
		
		// library name change listener
		libraryName.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) 
			{
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, library.getId() );
				library.setLibraryName(newValue);
				MasterController.getInstance().setLibraryToSave( library, libBooks, pendingDeleteBooks );

			}
		});
		
		// book combo box in dialog listener?
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			buttonSave.setDisable( true );
		}
		// else check the role
		// if session is admin
		if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL)
				||
			MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL))
		{
			buttonSave.setDisable( false );
			buttonInvReport.setDisable(false);
			buttonAddBook.setDisable(false);

			if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL) )
			{
				buttonDeleteBook.setDisable(true);
			}
			else
			{
				buttonDeleteBook.setDisable(false);

			}
		}
		
		else // intern or guest
		{
			buttonSave.setDisable( true);
			buttonInvReport.setDisable(true);
			buttonDeleteBook.setDisable(true);
			buttonAddBook.setDisable(true);

		}
	}
}
