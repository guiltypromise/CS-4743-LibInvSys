package controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import models.AuditTrailEntry;
import models.Author;
import models.Book;
import models.Library;
import models.LibraryBook;
import gateways.AuthorTableGateway;
import gateways.BookTableGateway;
import controllers.AuthorDetailController;
import gateways.GatewayException;
import gateways.LibraryBookTableGateway;
import gateways.LibraryTableGateway;
import controllers.AuthorListController;
import controllers.MasterController;
import enums.ViewType;

public class MasterController {
	private static final Logger logger = LogManager.getLogger(MasterController.class);

	
	private static MasterController instance = null;

	private BorderPane rootPane;

	private AuthorTableGateway authorGateway;
	
	private BookTableGateway bookGateway;
	
	private LibraryTableGateway libraryGateway;
	
	private LibraryBookTableGateway libraryBookGateway;
	
	private Author authorToSave;
	
	private Book bookToSave;
	
	private Library libraryToSave;
	
	private ArrayList<LibraryBook> pendSaveBooks;
	
	private ArrayList<LibraryBook> pendDeleteBooks;
	
	public AuthorListController authorListController;
	
	private AuthEJBRemote authBean;
	
	private int savedSessionId;
	
	private String sessionRole;
	

	
	/**
	 * State ID information for an added Author to prevent duplicate
	 * when edited that freshly added Author on the same Add Author form.
	 */
	private int lastSaved;
	
	/**
	 * State boolean for the AuthorDetail save confirmation modal.
	 */
	private boolean editedState;

	private MasterController() {
		editedState = false;
		//create gateways
		try {
			authorGateway = new AuthorTableGateway();
			bookGateway = new BookTableGateway();
			libraryGateway = new LibraryTableGateway();
			libraryBookGateway = new LibraryBookTableGateway();
			try {
				authBean = InitialContext.doLookup("LibInvSysEJB/AuthEJB!controllers.AuthEJBRemote");
				savedSessionId = AuthenticatorConstants.INVALID_SESSION;
				setSession( savedSessionId );
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (GatewayException e) {
			e.printStackTrace();
			this.close();
			Platform.exit();
		}
	}
		
	/**
	 * transition to new view in window's center area of border pane
	 * @param vType
	 * @param data
	 * @return
	 * @throws SQLException 
	 */
	public boolean changeView(ViewType vType, Object data, int sessionId) throws SQLException {
		FXMLLoader loader = null;

		// check if user did edit something but is changing views
		if( this.isEditedState() ) // if an edit happened but user didnt save
		{
			
			// check role here first!
			if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL)
					||
				MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL))
			{
				try {
					if( !confirmSaveDialog( data ) ) // if the save is cancelled
						return false; // bail out from changeView
				} catch (Exception e) {
					errorConfirmedDialog( e.getMessage() );
					logger.error( e.getMessage() );
					return false;
				} 
			}
		}
		
		// else load view appropriate to the given vType
		if(vType == ViewType.AUTHOR_LIST) {
			List<Author> authors = authorGateway.getAuthors();
			loader = new FXMLLoader(getClass().getResource("/views/authorlistgui.fxml"));
			loader.setController(new AuthorListController( authors, sessionId ));
		} else if(vType == ViewType.AUTHOR_DETAIL) {
			loader = new FXMLLoader(getClass().getResource("/views/authordetailview.fxml"));
			loader.setController(new AuthorDetailController((Author) data, sessionId ));
		} else if(vType == ViewType.BOOK_LIST ){
			List<Book> books = bookGateway.getBooks();
			loader = new FXMLLoader(getClass().getResource("/views/booklistview.fxml"));
			logger.info("Changing to Books List View");
			loader.setController(new BookListController( books, sessionId ));
		} else if (vType == ViewType.BOOK_DETAIL) {
			List<Author> authors = authorGateway.getAuthors();
			loader = new FXMLLoader(getClass().getResource("/views/bookdetailview.fxml"));
			loader.setController(new BookDetailController((Book) data, authors, sessionId));
		} else if(vType == ViewType.LIBRARY_LIST ){
			List<Library> libraries = libraryGateway.getLibraries();
			loader = new FXMLLoader(getClass().getResource("/views/librarylistview.fxml"));
			logger.info("Changing to Library List View");
			loader.setController(new LibraryListController( libraries, sessionId ));
		} else if (vType == ViewType.LIBRARY_DETAIL) {
			Library selected = (Library) data;
			List<LibraryBook> libraryBooks = selected.getLibraryBooks(); 
			loader = new FXMLLoader(getClass().getResource("/views/librarydetailview.fxml"));
			loader.setController(new LibraryDetailController(selected, libraryBooks, sessionId));
		} else if(vType == ViewType.AUDIT_VIEW) {
			Author author = null;
			Book book = null;
			Library library = null;
			List<AuditTrailEntry> auditEntries = null;
			int id = 0;
			
			loader = new FXMLLoader(getClass().getResource("/views/auditview.fxml"));

			
			// Now change to handle either a Book or an Author
			// check what data type we have from the Object
			if( data instanceof Book )
			{
				book = (Book) data;
				id = book.getId();
			}
			if( data instanceof Author )
			{
				author = (Author) data;
				id = author.getId();
			}
			if( data instanceof Library )
			{
				library = (Library) data;
				id = library.getId();
			}

			
			//if( !selected.validateFirstName(selected.getFirstName()))
			if (lastSaved > 0)
			{
				if( data instanceof Book )
				{
					try {
					book =  bookGateway.getBookById( id );
					auditEntries = book.getAuditTrail(); // grab the audit entries for this book
					loader.setController( new BookAuditTrailController( auditEntries, book, sessionId ) );	// load those audit entries into the controller for init
					} catch (Exception e) {
						//e.printStackTrace();
						logger.error("Failed to retrive audit entries for book");
					}
				}
				if( data instanceof Author )
				{
					author = authorGateway.getAuthorById( id );
					auditEntries = author.getAuditTrail(); // grab the audit entries for this author
					loader.setController( new AuthorAuditTrailController( auditEntries, author, sessionId ) );	// load those audit entries into the controller for init
				}
				if( data instanceof Library )
				{
					library = libraryGateway.getLibraryById( id );
					try {
						auditEntries = library.getAuditTrail();
					} catch (Exception e) {
						e.printStackTrace();
					} // grab the audit entries for this author
					loader.setController( new LibraryAuditTrailController( auditEntries, library, sessionId ) );	// load those audit entries into the controller for init
				}
			}
			else if( id <= 0 )
			{
				return false;
			}
			else
			{
				if( data instanceof Book )
				{
					book = bookGateway.getBookById( id );
					try {
						logger.info(String.format("Getting Audit Trail for Book [%s]", book.getId()));

						auditEntries = book.getAuditTrail();
					} catch (Exception e) {
						e.printStackTrace();
						logger.error("Unable to load book audit entries");
					} // grab the audit entries for this book
					loader.setController( new BookAuditTrailController( auditEntries, book, sessionId ) );	// load those audit entries into the controller for init

				}
				if( data instanceof Author )
				{
					author = authorGateway.getAuthorById( id );
					auditEntries = author.getAuditTrail(); // grab the audit entries for this author
					loader.setController( new AuthorAuditTrailController( auditEntries, author, sessionId ) );	// load those audit entries into the controller for init
				}
				if( data instanceof Library )
				{
					library = libraryGateway.getLibraryById(id);
					try {
						logger.info(String.format("Getting Audit Trail for Library [%s]", library.getId()));

						auditEntries = library.getAuditTrail();
					} catch (Exception e) {
												e.printStackTrace();
					}
					loader.setController( new LibraryAuditTrailController( auditEntries, library, sessionId ) );	// load those audit entries into the controller for init
				}
			}
			
		} else if(vType == ViewType.HOME_VIEW) {
			loader = new FXMLLoader(getClass().getResource("/views/home.fxml"));
		}
		
		Parent view = null;
		try { // try for view load
			view = loader.load(); 
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Unable to load view");
			return false;
		}
		//attach view to application center of border pane
		rootPane.setCenter(view);		
		return true;
	}

	/**
	 * clean up method to close gateways, etc.
	 */
	public void close() {
		authorGateway.close();
		bookGateway.close();
		libraryGateway.close();
	}
	
	public static MasterController getInstance() {
		if(instance == null)
			instance = new MasterController();
		return instance;
	}
	
	public BorderPane getRootPane() {
		return rootPane;
	}

	public void setRootPane(BorderPane rootPane) {
		this.rootPane = rootPane;
	}

	public AuthorTableGateway getAuthorGateway() {
		return authorGateway;
	}
	
	public BookTableGateway getBookGateway() {
		return bookGateway;
	}
	
	public LibraryTableGateway getLibraryGateway()
	{
		return libraryGateway;
	}
	
	public LibraryBookTableGateway getLibraryBookTableGateway()
	{
		return libraryBookGateway;
	}
	
	public void setLastSaved( int id )
	{
		this.lastSaved = id;
	}
	
	public int getLastSaved()
	{
		return this.lastSaved;
	}

	public boolean isEditedState() {
		return editedState;
	}

	public void setEditedState(boolean editedState, int id) {
		this.editedState = editedState;
		this.lastSaved = id;
	}
	
	// possibly change this to be Object data and use instanceof for different object types
	//public boolean confirmSaveDialog( Author confirmAuthor ) throws Exception
	public boolean confirmSaveDialog( Object data ) throws Exception
	{
		Author author = null;//authorGateway.getAuthorById( lastSaved );
		Book book = null;
		Library library = null;
		
		if ( data instanceof Author )
		{
			logger.info("FOUND Author type for saving");
			author = (Author) data;
		}
		if( data instanceof Book )
		{
			logger.info("FOUND Book type for saving");
			book = (Book) data;
		}
		if( data instanceof Library )
		{
			logger.info("FOUND Library type for saving");
			library = (Library) data;
		}

		// display modal dialog box for confirmation
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Confirm Save");
		alert.setHeaderText("You have unsaved changes.");
		alert.setContentText("Would you like to save?");
		
		ButtonType buttonTypeYes = new ButtonType("Yes", ButtonBar.ButtonData.YES );
		ButtonType buttonTypeNo = new ButtonType("No", ButtonBar.ButtonData.NO);
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(buttonTypeYes
					, buttonTypeNo
					, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes){
			logger.info("SAVE Button pressed");
			
			// prevent inserting duplicates and perform update instead
			// by getting state information from MasterController singleton
			if( this.getLastSaved() > 0 )
			{
				logger.info("FOUND last saved ID");

				if( data instanceof Author )
				{
					logger.info("FOUND Author data type last saved");
					author.setId( MasterController.getInstance().getLastSaved());	
				}
				else if( data instanceof Book ) 
				{
					logger.info("FOUND Book data type last saved");
					book.setId ( MasterController.getInstance().getLastSaved() );
				}
				else if( data instanceof Library )
				{
					logger.info("FOUND Library data type last saved");
					library.setId ( MasterController.getInstance().getLastSaved() );
				}
			
				//MasterController.getInstance().setLastSaved( 0 );
			}
			if( data instanceof Author )
			{
				logger.info("FOUND Author type for saving");

				if( !(this.authorToSave == null ) ) // if we have an existing author in buffer
				{		
					author = authorToSave; // grab it from the save buffer
				}
				String dob = null;
				if( author.getDateOfBirth() == null )
					logger.error("No dob selected");
				else
					dob = author.getDateOfBirth().toString();
				
				// save author method
				logger.info(" Saving " + author.getId() + " as ID");

				author.saveAuthor( author.getId() 
						//, firstName.getText()
						, author.getFirstName()
						, author.getLastName()
						, dob
						, author.getGender()
						, author.getWebsite() );
			}
			else if ( data instanceof Book )
			{
				logger.info("FOUND Book data type for saving");

				if( !(this.bookToSave == null ) ) // if we have an existing book in save buffer
				{		
					book = bookToSave; // grab it from save buffer
				}
				String datePublish = null;
				if( book.getDatePublished() == null )
					logger.error("No date published selected");
				else
					datePublish = book.getDatePublished().toString();
				
				// save book method
				logger.info(" Saving Book [" + book.getId() + "]");

				book.saveBook( book.getId() 
						//, firstName.getText()
						, book.getTitle()
						, book.getPublisher()
						, datePublish
						, book.getAuthor()
						, book.getSummary() );
			}
			else if ( data instanceof Library )
			{
				// TODO
				logger.info("FOUND Library data type for saving");
				if( !(this.libraryToSave == null ) ) // if we have an existing book in save buffer
				{		
					library = libraryToSave; // grab it from save buffer
				}
				library.saveLibrary( library.getId()
						, library.getLibraryName()
						, pendSaveBooks );
				if( pendDeleteBooks.size() > 0 ) 
				{
					getLibraryBookTableGateway().deleteLibraryBooks(library.getId()
							, pendDeleteBooks );
				}
				
			
			}
			saveConfirmedDialog();
		}
		else if ( result.get() == buttonTypeNo )
		{
			if( data instanceof Author )
				logger.info(" Did not save " + author.getId() + " as ID");
			else if( data instanceof Book )
				logger.info(" Did not save " + book.getId() + " as ID");
		}
		else {
			// else they cancelled, no action taken, and stay on current view
			if( data instanceof Author )
				logger.info(" Canceled Author Save" + author.getId() + " as ID");
			else if( data instanceof Book )
				logger.info(" Canceled Book Save" + book.getId() + " as ID");
			
			// dont reset edited state, it's still fair game when they leave and not save
			return false;
		}
		this.setEditedState( false, lastSaved ); // reset the edited state
		return true;
	}

	public Author getAuthorToSave() {
		return authorToSave;
	}
	
	public Book getBookToSave() {
		return bookToSave;
	}
	
	public Library getLibraryToSave() {
		return libraryToSave;
	}

	public void setAuthorToSave(Author authorToSave) {
		logger.info("Saving author in buffer for save confirm dialog");
		this.authorToSave = authorToSave;
	}
	
	public void setBookToSave(Book bookToSave) {
		logger.info("Saving Book in buffer for save confirm dialog");
		this.bookToSave = bookToSave;
	}
	
	public void setLibraryToSave(Library libraryToSave
			, List<LibraryBook> pendingSaveBooks
			, List<LibraryBook> pendingDeleteBooks) {
		logger.info("Saving Library in buffer for save confirm dialog");
		this.libraryToSave = libraryToSave;
		this.pendSaveBooks = (ArrayList<LibraryBook>) pendingSaveBooks;
		this.pendDeleteBooks = (ArrayList<LibraryBook>) pendingDeleteBooks;
		
	}
	
	public boolean getEditedState()
	{
		return editedState;
	}
	
	public void saveConfirmedDialog()
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Save Confirmed");
		alert.setHeaderText( "The record has been saved");			
		ButtonType buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE );
		alert.getButtonTypes().setAll(buttonTypeOK);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeOK)
		{
			logger.info("Save confirmed");	
			//return false;
		}
	}
	
	public void errorConfirmedDialog( String message )
	{
		Alert alert = new Alert(AlertType.ERROR);

		alert.setTitle("Error");
		alert.setHeaderText( message );
		//alert.setContentText("Would you like to save?");
		
		ButtonType buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE );
		
		alert.getButtonTypes().setAll(buttonTypeOK);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeOK)
		{
			logger.info("Error confirmed");	
		}
		
		logger.error(message);
	}
	
	public static Pair<String, String> loginDialog()
	{
		// Create the custom dialog.
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Login Dialog");
		dialog.setHeaderText("Enter user name and password");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField username = new TextField();
		username.setPromptText("Username");
		PasswordField password = new PasswordField();
		password.setPromptText("Password");

		grid.add(new Label("Username:"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label("Password:"), 0, 1);
		grid.add(password, 1, 1);

		// Enable/Disable login button depending on whether a username was entered.
		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		// Do some validation (using the Java 8 lambda syntax).
		username.textProperty().addListener((observable, oldValue, newValue) -> {
		    loginButton.setDisable(newValue.trim().isEmpty());
		});

		dialog.getDialogPane().setContent(grid);

		// Request focus on the username field by default.
		Platform.runLater(() -> username.requestFocus());

		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == loginButtonType) {
		        return new Pair<>(username.getText(), password.getText());
		    }
		    return null;
		});

		Optional<Pair<String, String>> result = dialog.showAndWait();

		if(!result.isPresent())
			return null;
					
		/*
		result.ifPresent(usernamePassword -> {
			return result.get();
		    //System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());
		});
		*/
		return result.get();
							
	}
	
	public AuthEJBRemote getAuthBean()
	{
		try {
			authBean = InitialContext.doLookup("LibInvSysEJB/AuthEJB!controllers.AuthEJBRemote");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return this.authBean;
	}
	
	public void setSession( int sessionId )
	{
		this.savedSessionId = sessionId;
		
		// also set the role
		setRole( savedSessionId );
	}
	
	public int getSessionId()
	{
		return this.savedSessionId;
	}
	
	private void setRole( int sessionId )
	{
		sessionRole = authBean.hasRole(sessionId);
	}
	
	public String getCurrentRole()
	{
		return sessionRole;
	}
}
