/**
 * AuthorAuditTrailController
 * @author Jason Martin
 * 
 * Description:
 * 		Controller logic for updating the AuditTrailView with 
 * 		information from the AuditTrailEntry model.
 * 
 * Notes:
 * 		
 */

package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import models.AuditTrailEntry;
import models.Author;

public class AuthorAuditTrailController implements Initializable
{
	/**
	 * Logger object for Log4j
	 */
	private static final Logger logger = LogManager.getLogger(AuthorAuditTrailController.class);

	private Author author;
	private List<AuditTrailEntry> entryList;
	
	@FXML private AnchorPane rootListPane;
	@FXML private ListView<AuditTrailEntry> auditList;
	@FXML private Label auditHeader;
	@FXML private Button backButton;
	private int savedSessionId;

	
	
	/**
	 * Constructs an auditTrailController with a list of Audit Entries.
	 * @param auditListEntries The list of AuditTrailEntry elements.
	 * @param selected 
	 * @param sessionId 
	 */
	public AuthorAuditTrailController( List<AuditTrailEntry> auditListEntries, Author selected, int sessionId) 
	{
		super();
		this.entryList = auditListEntries;
		this.author = selected;
		savedSessionId = sessionId;

	}

	@FXML private void handleBackAction (ActionEvent event ) throws IOException
	{
		try {
			logger.info("Back Button pressed");

			Author selected = MasterController.getInstance().getAuthorGateway().getAuthorById( author.getId() );
			MasterController.getInstance().changeView(ViewType.AUTHOR_DETAIL, selected, savedSessionId );
	
		} catch(Exception e ) {
			logger.error("IO AuditTrail Back Button error: " + e.getMessage());
		}	
	}
	
	/**
	 * Initialize the AuditView panel with data.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources ) 
	{
		logger.info("Init AuditView");
		
		auditHeader.setText( "Audit Trail for " + author.getFirstName() + " " + author.getLastName());

		ObservableList<AuditTrailEntry> elements = auditList.getItems();
		for( AuditTrailEntry entry : entryList )
		{
			logger.info("Adding audit trail entry to audit ListView");
			elements.add(entry);
		}
	}
}
