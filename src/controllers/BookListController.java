package controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import models.Book;

public class BookListController implements Initializable
{
	private static final Logger logger = LogManager.getLogger(BookListController.class);
	
	private List<Book> books;
	private List<Book> searchBooks;

	@FXML private ListView<Book> listView;
	@FXML private AnchorPane rootListPane;
	@FXML private Button deleteButton;
	@FXML private TextField titleField;
	@FXML private TextField publishField;
	@FXML private DatePicker dateField;
	
	private String titleSearch;
	private String publishSearch;
	private String dateSearch;
	private int savedSessionId;

	public BookListController( List<Book> books, int sessionId ) 
	{
		super();
		this.books = books;
		searchBooks = new ArrayList<Book>();
		titleSearch = "";
		publishSearch = "";
		dateSearch = "";
		savedSessionId = sessionId;
	}
	@FXML private void handleSearchAction( ActionEvent event ) throws IOException, Exception
	{
		logger.info("Clicked on Search Book button");
		
		ObservableList<Book> elements = listView.getItems();
		
		elements.clear();
		try {
		// apply list of books searched
		searchBooks = MasterController.getInstance().getBookGateway().searchBook(
				titleSearch
				, publishSearch
				, dateSearch);
		}
		catch (Exception e)
		{
			logger.error("BookList Search filter failure");
			e.printStackTrace();
		}
		
		// fill in the list view
		for( Book book : searchBooks )
		{
			elements.add(book);
		}

	}
	
	/**
	 * Handles book data and events the user selects from the ListView.
	 * @param event The mouse event performed.
	 * @throws IOException Throw an error if something about the item cannot be performed.
	 */
	@FXML private void handleListAction( MouseEvent event ) throws IOException 
	{
		try { 
			if( event.getClickCount() > 1 ) 
			{
				Book selected = listView.getSelectionModel().getSelectedItem();
				logger.info("Double clicked on " + selected.getTitle());
				MasterController.getInstance().changeView( ViewType.BOOK_DETAIL, selected, savedSessionId );
			}
		} catch ( Exception e ) {
			logger.error("IO List Double Click error: " + e.getMessage() );
		}
	}	
	
	@FXML private void handleDeleteAction( ActionEvent event ) throws Exception
	{
		Book selected = listView.getSelectionModel().getSelectedItem();

		logger.info("Pressed DELETE book button");
		
		// display modal dialog box for confirmation
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Confirm Deletion");
		alert.setHeaderText("Warning \'" + selected.getTitle() + "\' and associated library inventory data will be deleted.");
		alert.setContentText("Are you ok with this?");
		
		ButtonType buttonTypeOne = new ButtonType("Delete");
		ButtonType buttonTypeTwo = new ButtonType("Cancel");
		alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeOne){
			MasterController.getInstance().getBookGateway().deleteBook(selected);
			MasterController.getInstance().changeView(ViewType.BOOK_LIST, null, savedSessionId );
		}
		// else they cancelled, no action taken
	}
	
	/**
	 * Initialize the Book ListView panel with data.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		ObservableList<Book> elements = listView.getItems();
		for( Book book : books )
		{
			elements.add(book);
		}
		
		// set listeners on the GUI fields
		titleField.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				titleSearch = newValue;
			}
		});
		
		publishField.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				publishSearch = newValue;
			}
		});
		
		dateField.valueProperty().addListener( new ChangeListener<LocalDate>() 
		{

			@Override
			public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue,
					LocalDate newValue) {
				logger.info(" Date changed from " + oldValue + " to " + newValue);	
				if( newValue == null)
				{
					dateSearch = "";
				}
				else
				{
					dateSearch = newValue.toString();
				}
			}
		});
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			deleteButton.setDisable( true);
		}
		// else check the role
		// if session is admin
		if(  MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL) )
		{
			deleteButton.setDisable( false );
		}
		else // intern, librarian or guest
		{
			deleteButton.setDisable( true);
		}
	}
}
