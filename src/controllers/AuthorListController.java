/**
 * AuthorListController.java 
 * @author Jason Martin
 * 
 * Controls the backend logic for the AuthorListView GUI provided
 * by the AuthorListGUI.fxml
 * 
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import models.Author;

public class AuthorListController implements Initializable 
{
	private static final Logger logger = LogManager.getLogger(AuthorListController.class);
	
	private List<Author> authors;

	//@FXML private ListView<String> listView;	
	@FXML private ListView<Author> listView;	
	@FXML private AnchorPane rootListPane;
	@FXML private Button deleteButton;
	private int savedSessionId;
	
	public AuthorListController( List<Author> authors, int sessionId )
	{
		super();
		this.authors = authors;
		this.savedSessionId = sessionId;
	}
	
	/**
	 * Handles Author data and events the user selects from the ListView.
	 * @param event The mouse event performed.
	 * @throws IOException Throw an error if something about the item cannot be performed.
	 */
	@FXML private void handleListAction( MouseEvent event ) throws IOException 
	{
		try { 
			if( event.getClickCount() > 1 ) 
			{
				Author selected = listView.getSelectionModel().getSelectedItem();
				logger.info("Double clicked on " + selected.toString());
				MasterController.getInstance().changeView( ViewType.AUTHOR_DETAIL, selected, savedSessionId );
			}
		} catch ( Exception e ) {
			logger.error("IO List Double Click error: " + e.getMessage() );
		}
	}	
	
	@FXML private void handleDeleteAction( ActionEvent event ) throws Exception 
	{
		Author selected = listView.getSelectionModel().getSelectedItem();

		logger.info("Pressed the DELETE button for " + selected.getFirstName());
		try {
			
			// display modal dialog box for confirmation
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Confirm Deletion");
			alert.setHeaderText("Warning " + selected.getFirstName() + " " + selected.getLastName() + " will be deleted.");
			alert.setContentText("Are you ok with this?");
			
			ButtonType buttonTypeOne = new ButtonType("Delete");
			ButtonType buttonTypeTwo = new ButtonType("Cancel");
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne){
				try {
					MasterController.getInstance().getAuthorGateway().deleteAuthor(selected);
				}
				catch (Exception e) {
					MasterController.getInstance().errorConfirmedDialog( e.getMessage());
				}
				MasterController.getInstance().changeView(ViewType.AUTHOR_LIST, null, savedSessionId );
			}
			// else they cancelled, no action taken
		}
		catch( SQLException e)
		{
			logger.error("Failed to delete selected record from database");
		}
	}
	
	/**
	 * Initialize the Author ListView panel with data.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources ) 
	{
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			deleteButton.setDisable( true);
		}
		// else check the role
		// if session is admin
		if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL) )
		{
			deleteButton.setDisable( false );
		}
		else // intern, librarian or guest
		{
			deleteButton.setDisable( true);
		}
		
		
		ObservableList<Author> elements = listView.getItems();
		for( Author author : authors )
		{
			elements.add(author);
		}
	}

}
