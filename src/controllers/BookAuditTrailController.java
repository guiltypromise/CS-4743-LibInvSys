package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import models.AuditTrailEntry;
import models.Book;

public class BookAuditTrailController implements Initializable
{
	private static final Logger logger = LogManager.getLogger(BookAuditTrailController.class);
			
	@FXML private AnchorPane rootListPane;
	@FXML private ListView<AuditTrailEntry> auditList;
	@FXML private Label auditHeader;
	@FXML private Button backButton;
	
	private Book book;
	private List<AuditTrailEntry> entryList;
	private int savedSessionId;


	
	public BookAuditTrailController(List<AuditTrailEntry> auditEntries,
									Book book, int sessionId) 
	{
		super();
		this.entryList = auditEntries;
		this.book = book;
		savedSessionId = sessionId;
	}

	@FXML private void handleBackAction (ActionEvent event ) throws IOException
	{
		try {
			logger.info("Back Button pressed");

			Book selected = MasterController.getInstance().getBookGateway().getBookById( book.getId() );
			MasterController.getInstance().changeView(ViewType.BOOK_DETAIL, selected, savedSessionId );
	
		} catch(Exception e ) {
			logger.error("IO AuditTrail Back Button error: " + e.getMessage());
		}	
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.info("Init AuditView");
		
		auditHeader.setText( "Audit Trail for " + book.getTitle() );

		ObservableList<AuditTrailEntry> elements = auditList.getItems();
		for( AuditTrailEntry entry : entryList )
		{
			logger.info("Adding audit trail entry to audit ListView");
			elements.add(entry);
		}
		
	}

}
