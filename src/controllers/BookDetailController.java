package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import models.Author;
import models.Book;

public class BookDetailController implements Initializable
{
	private static final Logger logger = LogManager.getLogger(BookDetailController.class);
	
	@FXML private AnchorPane rootAnchorPane;
	@FXML private Label titleLabel;
	@FXML private Label errorLabel;
	@FXML private TextField bookTitle;
	@FXML private TextField bookPublisher;
	@FXML private TextArea bookSummary;
	@FXML private ComboBox<Author> authorSelect;
	@FXML private DatePicker datePublished;
	@FXML private Button auditViewButton;
	@FXML private Button saveButton;
	@FXML private Button backButton;
	@FXML private Button refreshButton;
	private Book book; //reference to a Book template
	private List<Author> authors; // a reference to all the authors in our library
	
	private int savedSessionId;


	public BookDetailController( Book book, List<Author> authors, int sessionId )
	{
		this.book = book; // save the selected Book reference
		this.authors = authors;
		savedSessionId = sessionId;
	}

	@FXML private void handleRefreshAction( ActionEvent event ) throws IOException 
	{
		logger.info("Refresh button pressed");

		try {
			Book selected = MasterController.getInstance().getBookGateway().getBookById( book.getId() );
			MasterController.getInstance().changeView(ViewType.BOOK_DETAIL, selected, savedSessionId );
		} catch (SQLException e) {
			logger.error("Refreshing operation failed");
			//e.printStackTrace();
		}
	}
	
	@FXML private void handleAuditViewAction( ActionEvent event ) throws IOException
	{
		logger.info("Audit Trail button pressed");
		try {
			MasterController.getInstance().changeView(ViewType.AUDIT_VIEW, book, savedSessionId );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error("IO Detail Audit Button error: " + e.getMessage());
		}	
	}
	
	@FXML private void handleSaveAction( ActionEvent event ) throws IOException
	{
		logger.info("Save button pressed");
		// prevent inserting duplicates and perform update instead
		// by getting state information from MasterController singleton
		if( MasterController.getInstance().getLastSaved() > 0 )
		{
			book.setId( MasterController.getInstance().getLastSaved());
			//MasterController.getInstance().setLastSaved( 0 );
		}
		String datePublish = null;
		if( book.getDatePublished() == null )
			logger.error("No date published selected");
		else
			datePublish = book.getDatePublished().toString();
		
		// save book method
		logger.info(" Saving Book [" + book.getId() + "]");

		try {
			book.saveBook( book.getId() 
					//, firstName.getText()
					, book.getTitle()
					, book.getPublisher()
					, datePublish
					, book.getAuthor()
					, book.getSummary() );
			
			if( book.getId() < 1 )
				book.setId(MasterController.getInstance().getLastSaved());
			this.book.setLastModified( MasterController.getInstance().getBookGateway().getBookById( book.getId() ).getLastModified());
			
			MasterController.getInstance().saveConfirmedDialog();

			// reset edited state since we saved here
			MasterController.getInstance().setEditedState( false, 0 ); 
		} catch (Exception e) {
			MasterController.getInstance().errorConfirmedDialog( e.getMessage());
			logger.error("IO Detail Save Button error: " + e.getMessage());
		}
	}
	
	@FXML private void handleBackAction( ActionEvent event ) throws IOException
	{
		logger.info("Back button pressed");
		try {
			
			if( MasterController.getInstance().getLastSaved() > 0 )
				book.setId( MasterController.getInstance().getLastSaved());
			
			// set the proxy author model values from the GUI fields to pass for saving if user wanted to commit
			//MasterController.getInstance().setAuthorToSave( returnAuthorToSave() );
			
			MasterController.getInstance().changeView(ViewType.BOOK_LIST, book, savedSessionId );		
		} catch(Exception e ) {
			logger.error("IO Detail Back Button error: " + e.getMessage());
		}
	}
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources ) 
	{
		logger.info("Loading Book Detail View...");
		
		// Initialize the Combo Box first
		ObservableList<Author> authorsList = authorSelect.getItems();
		
		if( book.getId() > 0 ) // if have an existing book
		{
			titleLabel.setText("Edit Book Details");
			datePublished.setValue( book.getDatePublished() );
			authorSelect.setValue( book.getAuthor() );
			auditViewButton.setVisible( true );
			refreshButton.setVisible( true );
			logger.info(String.format(" %s last_modified %s"
					, book.getId()
					, book.getLastModified()));
		}
		else // we will add a new book instead
		{
			titleLabel.setText("Add Book");
			authorSelect.setPromptText( "Select" );
		}
		
		// populate GUI fields
		bookTitle.setText( book.getTitle() );
		
		bookPublisher.setText( book.getPublisher() );
		
		bookSummary.setText( book.getSummary() );

		// set combo box for Author Selection
		for( Author author : authors )
		{
			authorsList.add( author ); 
		}
		
		// set listeners on the GUI fields
		bookTitle.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, book.getId() );
				book.setTitle( newValue );
				
				// Instead of setAuthorToSave we could change it to be some generic 
				// save method with Object data as an input like changeView and
				// decide using instanceof checks between Author and Book types
				MasterController.getInstance().setBookToSave(book);
			}
		});
		
		bookPublisher.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, book.getId() );
				book.setPublisher( newValue );
				MasterController.getInstance().setBookToSave(book);
			}
		});
		
		datePublished.valueProperty().addListener( new ChangeListener<LocalDate>() 
		{

			@Override
			public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue,
					LocalDate newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, book.getId() );
				book.setDatePublished( newValue.toString() );
				MasterController.getInstance().setBookToSave(book);
			}
		});
		
		authorSelect.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Author>() 
		{
			@Override
			public void changed(ObservableValue<? extends Author> observable, Author oldValue, Author newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, book.getId() );
				book.setAuthor(newValue);
				MasterController.getInstance().setBookToSave(book);
			}
		});
		
		bookSummary.textProperty().addListener( new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable
								, String oldValue
								, String newValue) {
				logger.info(" Value changed from " + oldValue + " to " + newValue);	
				MasterController.getInstance().setEditedState( true, book.getId() );
				book.setSummary( newValue );
				MasterController.getInstance().setBookToSave(book);
			}
		});
		
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			saveButton.setDisable( true);
		}
		// else check the role
		// if session is admin
		if( MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL)
				||
			MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.LIBRARIAN_LEVEL))
		{
			saveButton.setDisable( false );
		}
		else // intern or guest
		{
			saveButton.setDisable( true);
		}
	}
}
