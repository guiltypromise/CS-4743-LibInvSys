package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.ViewType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import models.Author;
import models.Library;

public class LibraryListController implements Initializable
{
	private static final Logger logger = LogManager.getLogger(LibraryListController.class);
	private List<Library> libraries;
	
	// @FXML
	@FXML private ListView<Library> listView;
	@FXML private AnchorPane rootListPane;
	@FXML private Button deleteButton;
	private int savedSessionId;

	public LibraryListController( List<Library> libraries, int sessionId ) 
	{
		super();
		this.libraries = libraries;
		savedSessionId = sessionId;
	}
	
	@FXML private void handleListAction( MouseEvent event ) throws IOException
	{
		try { 
			if( event.getClickCount() > 1 ) 
			{
				Library selected = listView.getSelectionModel().getSelectedItem();
				selected = MasterController.getInstance().getLibraryGateway().getLibraryById(selected.getId());
				logger.info("Double clicked on " + selected.toString());
				MasterController.getInstance().changeView( ViewType.LIBRARY_DETAIL, selected, savedSessionId );
			}
		} catch ( Exception e ) {
			logger.error("IO List Double Click error: " + e.getMessage() );
		}		
	}
	
	/**
	 * Delete a Library and it's LibraryBook inventory
	 * @param event
	 * @throws Exception
	 */
	@FXML private void handleDeleteAction( ActionEvent event ) throws Exception 
	{
		Library selected = listView.getSelectionModel().getSelectedItem();

		logger.info("Pressed the DELETE button for " + selected.getLibraryName());
		try {
			
			// display modal dialog box for confirmation
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Confirm Deletion");
			alert.setHeaderText("Warning \'" + selected.getLibraryName() + "\' and associated inventory data will be deleted.");
			alert.setContentText("Are you ok with this?");
			
			ButtonType buttonTypeOne = new ButtonType("Delete");
			ButtonType buttonTypeTwo = new ButtonType("Cancel");
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne){
				try {
					MasterController.getInstance().getLibraryGateway().deleteLibrary(selected);
				}
				catch (Exception e) {
					MasterController.getInstance().errorConfirmedDialog( e.getMessage());
				}
				MasterController.getInstance().changeView(ViewType.LIBRARY_LIST, null, savedSessionId );
			}
			// else they cancelled, no action taken
		}
		catch( SQLException e)
		{
			logger.error("Failed to delete selected record from database");
		}
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		if( savedSessionId == AuthenticatorConstants.INVALID_SESSION )
		{
			deleteButton.setDisable( true);
		}
		// else check the role
		// if session is admin
		if(  MasterController.getInstance().getCurrentRole().equals(AuthenticatorConstants.ADMIN_LEVEL))
		{
			deleteButton.setDisable( false );
		}
		else // intern, librarian or guest
		{
			deleteButton.setDisable( true);
		}

		ObservableList<Library> elements = listView.getItems();
		for(Library library : libraries )
		{
			elements.add(library);
		}		
	}

}
