package controllers;

public abstract class AuthenticatorConstants {
	public static final int INVALID_SESSION = 0;

	//fine-grained functional access
	public static final String ADMIN_LEVEL = "admin";
	public static final String LIBRARIAN_LEVEL = "librarian";
	public static final String INTERN_LEVEL = "intern";

}
