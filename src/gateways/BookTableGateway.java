package gateways;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import controllers.MasterController;
import models.AuditTrailEntry;
import models.Author;
import models.Book;

public class BookTableGateway
{
	private static final Logger logger = LogManager.getLogger(BookTableGateway.class);

	// save the connection here
	private Connection conn;
	
	
	/**
	 * Creates an instance of the BookTableGateway and connects to the 
	 * Book table inside the database.
	 * @throws GatewayException
	 */
	public BookTableGateway()  throws GatewayException
	{
		conn = null;
		try
		{
			Properties props = new Properties();
			FileInputStream stream = new FileInputStream("db.properties");
	        props.load(stream);
	        stream.close();
			
			// Create the data source
			MysqlDataSource ds = new MysqlDataSource();
	        ds.setURL(props.getProperty("MYSQL_DB_URL"));
	        ds.setUser(props.getProperty("MYSQL_DB_USERNAME"));
	        ds.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
	        
	        //create the connection
	        logger.info( "BookTableGateway connecting to " + ds.getURL() );
			conn = ds.getConnection();
			
		} catch( IOException | SQLException e ) {
			e.printStackTrace();
			throw new GatewayException(e);
		}
	}
	
	/**
	 * Closes the database connection
	 */
	public void close()
	{
		if(conn != null) {
			try {
		        logger.info( "Closing connection" );
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("Unable to close the BookGateway connection");

				//e.printStackTrace();
			}
		}
	}
	
	public List<Book> searchBook( String title, String publisher, String date) throws SQLException
	{
		logger.info("Searching book from database");
		
		Author author = null;
		Book book = null;
		List<Book> books = new ArrayList<Book>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		StringBuilder titleSearch = new StringBuilder();
		StringBuilder publishSearch = new StringBuilder();
		StringBuilder dateSearch = new StringBuilder();
		
		titleSearch.append(title.trim());
		titleSearch.append("%");
		publishSearch.append(publisher.trim());
		publishSearch.append("%");
		dateSearch.append(date);
		
		try 
		{
			conn.setAutoCommit(false);
			
			// do search stuff
//			SELECT * 
//			FROM  `books` 
//			WHERE  `title` LIKE  '%'
//			AND  `publisher` LIKE  'Sch%'
//			AND  `date_published` >=  '2016-12-14'
			
			//SELECT * FROM authors as a INNER JOIN books as b ON b.author_id=a.id  
			//WHERE b.title like '%' ORDER BY b.id
					
			if( dateSearch.toString().isEmpty() ) // no specified date?
			{
				st = conn.prepareStatement( "SELECT * "
						+ "FROM authors as a "
						+ "INNER JOIN books as b "
						+ "ON b.author_id=a.id "
						+ "WHERE b.title LIKE ? "
						+ "AND b.publisher LIKE ? "
						+ "ORDER BY b.id");
				
				st.setString(1, titleSearch.toString());
				st.setString(2, publishSearch.toString());

			}
			else // otherwise have a date title and publisher search + wildcard '%'
			{
				//st = conn.prepareStatement( "SELECT * FROM books where title like ? "
				//		+ "AND publisher like ? "
				//		+ "AND date_published >= ?");
				
				st = conn.prepareStatement( "SELECT * "
						+ "FROM authors as a "
						+ "INNER JOIN books as b "
						+ "ON b.author_id=a.id "
						+ "WHERE b.title LIKE ? "
						+ "AND b.publisher LIKE ? "
						+ "AND date_published >= ?"
						+ "ORDER BY b.id");
				st.setString(1, titleSearch.toString());
				st.setString(2, publishSearch.toString());
				st.setString(3, dateSearch.toString());
			}
			
			// execute the query
			
			rs = st.executeQuery();
			
			while( rs.next() )
			{
				// set the Author object
				author = new Author( rs.getString("first_name") 
						, rs.getString("last_name"));
				author.setId( rs.getInt(1));
				author.setDateOfBirth( rs.getString("dob"));
				author.setGender( rs.getString("gender"));
				author.setWebsite( rs.getString("web_site"));
				author.setLastModified( rs.getTimestamp(7).toLocalDateTime());
				
				// Set the Book object
				book = new Book();
				book.setId( rs.getInt(8));
				book.setTitle(rs.getString("title"));
				book.setPublisher(rs.getString("publisher"));
				book.setDatePublished( rs.getString("date_published"));
				book.setSummary(rs.getString("summary"));
				book.setLastModified( rs.getTimestamp(14).toLocalDateTime());
				
				// set the author into the book
				book.setAuthor( author );

				// add the book to the list of Books for return
				books.add(book);
			}
			
			
			conn.commit(); // complete transaction
		}
		catch ( SQLException e)
		{
			logger.error("Failed execute search query" );
			conn.rollback(); // cancel transaction
		}
		finally 
		{
			conn.setAutoCommit( true ); // unlock the record
		}
		return books;
	}
	
	
	public void deleteBook( Book book ) throws SQLException
	{
		@SuppressWarnings("unused")
		ResultSet rs = null;
		PreparedStatement st = null;
		logger.info("Deleting book from database");
		try {
			conn.setAutoCommit( false ); // begin transaction, lock the record

			st = conn.prepareStatement("DELETE FROM audit_trail WHERE record_id=? AND record_type = 'B'");
			st.setInt(1, book.getId() ); // get parameterized book ID
			st.executeUpdate();
			
			st = conn.prepareStatement("DELETE FROM library_book WHERE book_id = ?");
			st.setInt(1,  book.getId() );
			st.executeUpdate();
			
			st = conn.prepareStatement("DELETE FROM books WHERE id = ?" );
			st.setInt(1, book.getId() ); // get parameterized book ID
			st.executeUpdate();

			conn.commit(); // complete transaction
		}
		catch ( SQLException e ) {
			logger.error("Failed to delete book record" );
			conn.rollback(); // cancel transaction
		}
		finally {
			conn.setAutoCommit( true ); // unlock the record
		}
	}
	/**
	 * Returns a List of Books from the database.
	 * @return A List of Books.
	 */
	public List<Book> getBooks() throws SQLException
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		Author author = null;
		Book book = null;
		List<Book> books = new ArrayList<Book>();
		try 
		{
			// get a JOINed result set of Books with their Authors
			st = conn.prepareStatement("SELECT * FROM authors AS a "
					+ "INNER JOIN books AS b "
					+ "ON b.author_id=a.id ORDER BY b.id ");
			rs = st.executeQuery();
			
			while( rs.next() )
			{
				// set the Author object
				author = new Author( rs.getString("first_name") 
						, rs.getString("last_name"));
				author.setId( rs.getInt(1));
				author.setDateOfBirth( rs.getString("dob"));
				author.setGender( rs.getString("gender"));
				author.setWebsite( rs.getString("web_site"));
				author.setLastModified( rs.getTimestamp(7).toLocalDateTime());
				
				// Set the Book object
				book = new Book();
				book.setId( rs.getInt(8));
				book.setTitle(rs.getString("title"));
				book.setPublisher(rs.getString("publisher"));
				book.setDatePublished( rs.getString("date_published"));
				book.setSummary(rs.getString("summary"));
				book.setLastModified( rs.getTimestamp(14).toLocalDateTime());
				
				// set the author into the book
				book.setAuthor( author );

				// add the book to the list of Books for return
				books.add(book);
			}
		} catch ( SQLException e ) {
			logger.error(" getBooks() failed to retrieve.");
		} finally {
			if(rs != null )
				rs.close();
			if( st !=null )
				st.close();
		}
		
		return books;
	}

	public Book getBookById(int id) {
		ResultSet rs = null;
		PreparedStatement st = null;
		Book book = null;
		Author author = null;
		try
		{
			logger.info("Retrieving book with ID = " + id);
			st = conn.prepareStatement("SELECT * from authors AS a INNER JOIN books AS b ON "
					+ "b.id=? AND b.author_id=a.id");
			st.setInt(1, id);
			rs = st.executeQuery();
			while( rs.next() )
			{
				author = new Author( rs.getString("first_name") 
						, rs.getString("last_name"));
				author.setId( rs.getInt(1));
				author.setDateOfBirth( rs.getString("dob"));
				author.setGender( rs.getString("gender"));
				author.setWebsite( rs.getString("web_site"));
				author.setLastModified( rs.getTimestamp(7).toLocalDateTime());
				
				book = new Book(rs.getString("title"));
				//System.out.println("First " + author.getFirstName() );
				book.setId( rs.getInt(8));
				book.setPublisher( rs.getString("publisher"));
				book.setDatePublished( rs.getString("date_published"));
				book.setSummary( rs.getString("summary"));
				book.setAuthor(author);
				book.setLastModified( rs.getTimestamp(14).toLocalDateTime());
				
				logger.info(String.format("Old Book %s last_modified %s"
							, book.getId()
							, book.getLastModified()));
			}
		}
		catch ( SQLException e )
		{
			logger.error("Failed to fetch Book for ID = " + id);
		}
		
		return book;
	}
	
	@SuppressWarnings("static-access")
	public void insertBook(Book book) throws SQLException  {
		ResultSet rs = null;
		PreparedStatement st = null;
		int currentSaveId = 0;
		logger.info("Inserting into database");
		try {
			st = conn.prepareStatement("INSERT INTO books"
					+ "( id,title, publisher, date_published, summary, author_id, last_modified) "
					+ "VALUES ( NULL, ?, ?, ?, ? ,?, CURRENT_TIMESTAMP)" , st.RETURN_GENERATED_KEYS);
			
			conn.setAutoCommit( false ); // begin transaction

			st.setString(1,  book.getTitle());
			st.setString(2,  book.getPublisher());
			st.setString(3, book.getDatePublished().toString());
			st.setString(4, book.getSummary());
			st.setInt(5, book.getAuthor().getId());
			
			st.executeUpdate();
			
			rs = st.getGeneratedKeys();
			while( rs.next () ) { // save the inserted record's id to avoid duplicate inserts
				
				MasterController.getInstance().setLastSaved( rs.getInt(1) );
				currentSaveId =  MasterController.getInstance().getLastSaved();
				logger.info("Saving Book ID: " + MasterController.getInstance().getLastSaved());
			}
			
			st = conn.prepareStatement("INSERT INTO audit_trail "
				+ "( id ,record_type ,record_id , date_added , entry_msg) "
				+ " VALUES (NULL , 'B',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'B' for Book record_type
			st.setInt( 1, currentSaveId );
			st.setString( 2, "Added");
			
			st.executeUpdate();
			
			conn.commit(); // complete transaction
		}
		catch (SQLException e){
			e.printStackTrace();
			logger.error("Failed to insert Book record" );
			conn.rollback();
		} 
		finally {
			conn.setAutoCommit( true ); // finally unlock the record
		}		
	}

	public void updateBook(Book book) throws Exception
	{
		Book oldBook = getBookById( book.getId() ); // grab the old author record from database
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		@SuppressWarnings("unused")
		ResultSet rs = null;
		PreparedStatement st = null;
		PreparedStatement ast = null;
		logger.info("Updating Book ID " + oldBook.getId() +" into database");
		logger.info(String.format("New Book %s last_modified %s"
				, book.getId()
				, book.getLastModified()));
		try {
			st = conn.prepareStatement("UPDATE books SET "
					+ "title = ?"
					+ ", publisher = ? "
					+ ", date_published = ?"
					+ ", author_id = ?"
					+ ", summary = ?"
					//+ ", last_modified = ?"
					+ " where id = ?");

			conn.setAutoCommit( false ); // begin transaction
			
			// parameterized updates from passed model reference
			
			// if the current first name is different from the one in the database before updating...
			if( !oldBook.getTitle().equals( book.getTitle()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setRecordDescriptor( book.getTitle());
				entry.setMessage(String.format("Title changed from \'%s\' to \'%s\'"
									, oldBook.getTitle(), book.getTitle()) );
				entries.add( entry );
			}
			st.setString(1,  book.getTitle()); // insert into parameter for the prepped update query
			
			// check publisher
			if( !oldBook.getPublisher().equals( book.getPublisher()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setRecordDescriptor( book.getTitle());
				entry.setMessage(String.format("Publisher changed from \'%s\' to \'%s\'"
									, oldBook.getPublisher(), book.getPublisher()) );
				entries.add( entry );
			}
			st.setString(2,  book.getPublisher());
			
			// check date published
			if( !oldBook.getDatePublished().toString().equals( book.getDatePublished().toString()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setRecordDescriptor( book.getTitle());
				entry.setMessage(String.format("Date Published changed from \'%s\' to \'%s\'"
									, oldBook.getDatePublished().toString()
									, book.getDatePublished().toString()) );
				entries.add( entry );
			}
			st.setString(3, book.getDatePublished().toString());
			
			// check author ( their ID )
			if( !(oldBook.getAuthor().getId() == book.getAuthor().getId()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setRecordDescriptor( book.getTitle());
				entry.setMessage(String.format("Author changed from \'%s\' to \'%s\'"
									, oldBook.getAuthor().toString(), book.getAuthor().toString()) );
				entries.add( entry );
			}
			st.setInt(4, book.getAuthor().getId());
			
			if( !oldBook.getSummary().equals( book.getSummary()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setRecordDescriptor( book.getTitle());
				entry.setMessage(String.format("Summary changed from \'%s\' to \'%s\'"
									, oldBook.getSummary(), book.getSummary()) );
				entries.add( entry );
			}
			st.setString(5,  book.getSummary());
			
			st.setInt( 6, book.getId() );
			
			//st.setTimestamp(7, Timestamp.valueOf( author.getLastModified()) );
			
			// compare the old author's timestamp with new author timestamp
			if( !oldBook.getLastModified().equals( book.getLastModified() ))
			{ 
				logger.warn( "last_modified timestamps do not match" );
				conn.rollback(); // cancel transaction
				throw new Exception( "Someone else has saved before you."
						+ " \nPlease refresh or go back to List and re-select for most recently saved details." );
			}
			
			st.executeUpdate();
			
			// for each audit message, insert into database
			for( AuditTrailEntry entry : entries )
			{
				
					ast = conn.prepareStatement("INSERT INTO audit_trail "
						+ "( id ,record_type ,record_id , date_added , entry_msg) "
						+ " VALUES (NULL , 'B',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'B' for Book record_type
					ast.setInt( 1, book.getId() );
					ast.setString( 2, entry.getMessage());
					ast.executeUpdate();
			}
			
			conn.commit(); // complete transaction
		} 
		catch (SQLException e) 
		{
			//e.printStackTrace();
			logger.error("Failed to update record" );
			// rollback transaction on error
			conn.rollback();
		}
		finally {
			conn.setAutoCommit( true ); // finally unlock the record
		}
	}
	
	public List<AuditTrailEntry> getBookAuditEntries( Book book ) throws SQLException
	{
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			st = conn.prepareStatement(
					  "SELECT * FROM books AS b, audit_trail AS at "
					+ "WHERE b.id=at.record_id AND b.id=? AND record_type='B'"
					+ "ORDER BY at.date_added" );
			st.setInt( 1, book.getId() ); // get parameterized Book ID
			logger.info("Fetching audit trail for: " +  book.getTitle());
			rs = st.executeQuery();
			while(rs.next() ){
				AuditTrailEntry entry = new AuditTrailEntry();
				logger.info("Fetching date_added ");

				entry.setRecordDescriptor( book.getTitle() );
				entry.setDateAdded( rs.getTimestamp( "date_added" ) ); // in table its Timestamp type
				entry.setMessage( rs.getString( "entry_msg"));
				//entry.setRecordDescriptor( "Record ID: " + rs.getInt("record_id" ) 
				//					   + ", Record Type: " + rs.getString("record_type"));
				logger.info("Adding entry with " + entry.getRecordDescriptor());
				entries.add( entry );
			}
		}
		catch ( SQLException e) {	
			logger.error("SQL getAuthorAuditEntries() failed");
			e.printStackTrace();
		}
		return entries;
	}
}
