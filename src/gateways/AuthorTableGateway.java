/**
 * AuthorTableGateway
 * 
 * Purpose:
 * 		Provides a gateway to a resulting table from the database for
 * 		model data.
 * 
 * 		Searches, updates, inserts data to/from database
 * 
 * 		
 */
package gateways;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import controllers.MasterController;
import core.Launcher;
import models.AuditTrailEntry;
import models.Author;

public class AuthorTableGateway 
{
	private static final Logger logger = LogManager.getLogger(Launcher.class);

	// save the connection here
	private Connection conn;
	
	/**
	 * Creates an instance of the AuthorTableGateway and connects to the
	 * database.
	 * @throws GatewayException
	 */
	public AuthorTableGateway() throws GatewayException
	{
		conn = null;
		// connect to data source and create a connection instance object
		try
		{
			Properties props = new Properties();
			FileInputStream stream = new FileInputStream("db.properties");
	        props.load(stream);
	        stream.close();
			
			// Create the data source
			MysqlDataSource ds = new MysqlDataSource();
	        ds.setURL(props.getProperty("MYSQL_DB_URL"));
	        ds.setUser(props.getProperty("MYSQL_DB_USERNAME"));
	        ds.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
	        
	        //create the connection
	        logger.info( "AuthorTableGateway connecting to " + ds.getURL() );
			conn = ds.getConnection();
			
		} catch( IOException | SQLException e ) {
			e.printStackTrace();
			throw new GatewayException(e);
		}
	}
	
	/**
	 * Closes the database connection
	 */
	public void close()
	{
		if(conn != null) {
			try {
		        logger.info( "Closing connection" );
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("Unable to close the AuthorGateway connection");
				//e.printStackTrace();
			}
		}
	}
	
	public Author getAuthorById( int id ) throws SQLException
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		Author author = null;
		try
		{
			st = conn.prepareStatement("SELECT * from authors WHERE id=?");
			st.setInt(1, id);
			rs = st.executeQuery();
			while( rs.next() )
			{
				author = new Author(rs.getString("first_name")
						 , rs.getString("last_name"));
				//System.out.println("First " + author.getFirstName() );
				author.setId( rs.getInt("id"));
				author.setGender( rs.getString("gender"));
				author.setDateOfBirth( rs.getString("dob"));
				author.setWebsite( rs.getString("web_site"));
				author.setLastModified( rs.getTimestamp("last_modified").toLocalDateTime());
				
				logger.info(String.format("Old Author %s last_modified %s"
							, author.getId()
							, author.getLastModified()));
			}
		}
		catch ( SQLException e )
		{
			logger.error("Failed to fetch author for ID = " + id);
		}
		
		return author;
	}
	
	/**
	 * Updates the database entry with the new author data.
	 * @param author A reference to the new author data model.
	 * @throws Exception 
	 */
	public void updateAuthor( Author author ) throws Exception 
	{
		Author oldAuthor = getAuthorById( author.getId() ); // grab the old author record from database
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		@SuppressWarnings("unused")
		ResultSet rs = null;
		PreparedStatement st = null;
		PreparedStatement ast = null;
		logger.info("Updating ID " + author.getId() +" into database");
		logger.info(String.format("New Author %s last_modified %s"
				, author.getId()
				, author.getLastModified()));
		try {
			st = conn.prepareStatement("UPDATE authors SET "
					+ "first_name = ?"
					+ ", last_name = ? "
					+ ", dob = ?"
					+ ", gender = ?"
					+ ", web_site = ?"
					//+ ", last_modified = ?"
					+ " where id = ?");

			conn.setAutoCommit( false ); // begin transaction
			
			// parameterized updates from passed model reference
			
			// if the current first name is different from the one in the database before updating...
			if( !oldAuthor.getFirstName().equals( author.getFirstName()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry(author.getFirstName(), author.getLastName());
				entry.setMessage(String.format("First Name changed from \'%s\' to \'%s\'"
									, oldAuthor.getFirstName(), author.getFirstName()) );
				entries.add( entry );
			}
			st.setString(1,  author.getFirstName());
			
			if( !oldAuthor.getLastName().equals( author.getLastName()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry( author.getFirstName(), author.getLastName());
				entry.setMessage(String.format("Last Name changed from \'%s\' to \'%s\'"
									, oldAuthor.getLastName(), author.getLastName()) );
				entries.add( entry );
			}
			st.setString(2,  author.getLastName());
			
			if( !oldAuthor.getDateOfBirth().toString().equals( author.getDateOfBirth().toString()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry(author.getFirstName(), author.getLastName());
				entry.setMessage(String.format("DOB changed from \'%s\' to \'%s\'"
									, oldAuthor.getDateOfBirth().toString(), author.getDateOfBirth().toString()) );
				entries.add( entry );
			}
			st.setString(3, author.getDateOfBirth().toString());
			
			if( !oldAuthor.getGender().equals( author.getGender()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry(author.getFirstName(), author.getLastName());
				entry.setMessage(String.format("Gender changed from \'%s\' to \'%s\'"
									, oldAuthor.getGender(), author.getGender()) );
				entries.add( entry );
			}
			st.setString(4, author.getGender());
			
			if( !oldAuthor.getWebsite().equals( author.getWebsite()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry(author.getFirstName(), author.getLastName());
				entry.setMessage(String.format("Website changed from \'%s\' to \'%s\'"
									, oldAuthor.getWebsite(), author.getWebsite()) );
				entries.add( entry );
			}
			st.setString(5, author.getWebsite());
			
			st.setInt( 6, author.getId() );
			
			//st.setTimestamp(7, Timestamp.valueOf( author.getLastModified()) );
			
			// compare the old author's timestamp with new author timestamp
			if( !oldAuthor.getLastModified().equals( author.getLastModified() ))
			{ 
				logger.warn( "last_modified timestamps do not match" );
				conn.rollback(); // cancel transaction
				throw new Exception( "Someone else has saved before you."
						+ " \nPlease refresh or go back to List and re-select for most recently saved details." );
			}
			
			
			st.executeUpdate();
			
			// for each audit message, insert into database
			for( AuditTrailEntry entry : entries )
			{
				ast = conn.prepareStatement("INSERT INTO audit_trail "
						+ "( id ,record_type ,record_id , date_added , entry_msg) "
						+ " VALUES (NULL , 'A',  ?, CURRENT_TIMESTAMP , ? )" );
					ast.setInt( 1, author.getId() );
					ast.setString( 2, entry.getMessage());
					ast.executeUpdate();
			}
			
			conn.commit(); // complete transaction
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("Failed to update record" );
			// rollback transaction on error
			conn.rollback();
		}
		finally {
			conn.setAutoCommit( true ); // finally unlock the record
		}
	} 
	
	@SuppressWarnings("static-access")
	public void insertAuthor(Author author) throws SQLException
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		int currentSaveId = 0;
		logger.info("Inserting into database");
		try {
			st = conn.prepareStatement("INSERT INTO authors"
					+ "( id, first_name, last_name, dob, gender, web_site, last_modified) "
					+ "VALUES ( NULL, ?, ?, ?, ? ,?, CURRENT_TIMESTAMP)" , st.RETURN_GENERATED_KEYS);
			
			conn.setAutoCommit( false ); // begin transaction

			st.setString(1,  author.getFirstName());
			st.setString(2,  author.getLastName());
			st.setString(3, author.getDateOfBirth().toString());
			st.setString(4, author.getGender());
			st.setString(5, author.getWebsite());
			
			st.executeUpdate();
			
			rs = st.getGeneratedKeys();
			while( rs.next () ) { // save the inserted record's id to avoid duplicate inserts
				
				MasterController.getInstance().setLastSaved( rs.getInt(1) );
				currentSaveId =  MasterController.getInstance().getLastSaved();
				logger.info("Saving Author ID: " + MasterController.getInstance().getLastSaved());
			}
			st = conn.prepareStatement("INSERT INTO audit_trail "
				+ "( id ,record_type ,record_id , date_added , entry_msg) "
				+ " VALUES (NULL , 'A',  ?, CURRENT_TIMESTAMP , ? )" );
			st.setInt( 1, currentSaveId );
			st.setString( 2, "Added");
			
			st.executeUpdate();
			
			conn.commit(); // complete transaction
		}
		catch (SQLException e){
			logger.error("Failed to insert record" );
			conn.rollback();
		} 
		finally {
			conn.setAutoCommit( true ); // finally unlock the record
		}
	}
	
	public void deleteAuthor( Author author ) throws Exception
	{
		@SuppressWarnings("unused")
		ResultSet rs = null;
		PreparedStatement st = null;
		logger.info("Deleting from database");
		try {
			conn.setAutoCommit( false ); // begin transaction, lock the record

			st = conn.prepareStatement("DELETE FROM audit_trail WHERE record_id=?");
			st.setInt(1, author.getId() ); // get parameterized author ID
			st.executeUpdate();
			
			st = conn.prepareStatement("DELETE FROM authors WHERE id = ?" );
			st.setInt(1, author.getId() ); // get parameterized author ID
			st.executeUpdate();

			conn.commit(); // complete transaction
		}
		catch ( SQLException e ) {
			logger.error("Failed to delete record and audit trail" );
			conn.rollback();
			throw new Exception( String.format("All books from %s %s must be deleted first"
					, author.getFirstName(), author.getLastName()));
		}
		finally {
			conn.setAutoCommit( true ); // unlock the record
		}
	}
	
	/**
	 * Returns a list of Authors from the Authors table
	 * @return
	 * @throws SQLException Error when reading from the database fails.
	 */
	public List<Author> getAuthors() throws SQLException
	{
		List<Author> authors = new ArrayList<Author>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		// use prepared statements, parameterized queries here
		try {
			st = conn.prepareStatement("SELECT * FROM authors");
			rs = st.executeQuery();
			while(rs.next() ){
				Author author = new Author(rs.getString("first_name")
										 , rs.getString("last_name"));
				author.setId( rs.getInt("id"));
				author.setGender( rs.getString("gender"));
				author.setDateOfBirth( rs.getString("dob"));
				author.setWebsite( rs.getString("web_site"));
				try { // not too proud of this hack to see invalid timestamp SQL dates
					author.setLastModified( rs.getTimestamp("last_modified").toLocalDateTime());
				}
				catch (Exception e)
				{
					author.setLastModified(null);
				}
				authors.add(author);
			}
		}
		catch (SQLException e)
		{
			logger.error("SQL getAuthors() failed");
			//e.printStackTrace();
		} finally {
			if(rs != null )
				rs.close();
			if( st !=null )
				st.close();
		}
		return authors;
	}

	public List<AuditTrailEntry> getAuthorAuditEntries( Author author ) throws SQLException
	{
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			st = conn.prepareStatement(
					  "SELECT * FROM authors AS a, audit_trail AS at "
					+ "WHERE a.id=at.record_id AND a.id=? AND record_type='A'"
					+ "ORDER BY at.date_added" );
			st.setInt( 1, author.getId() ); // get parameterized author ID
			logger.info("Fetching audit trail for: " +  author.toString());
			rs = st.executeQuery();
			while(rs.next() ){
				AuditTrailEntry entry = new AuditTrailEntry(author.getFirstName(), author.getLastName());
				//logger.info("Fetching date_added ");

				entry.setDateAdded( rs.getTimestamp( "date_added" ) ); // in table its Timestamp type
				entry.setMessage( rs.getString( "entry_msg"));
				//entry.setRecordDescriptor( "Record ID: " + rs.getInt("record_id" ) 
				//					   + ", Record Type: " + rs.getString("record_type"));
				logger.info("Adding entry with " + entry.getRecordDescriptor());
				entries.add( entry );
			}
		}
		catch ( SQLException e) {	
			logger.error("SQL getAuthorAuditEntries() failed");
			e.printStackTrace();
		}
		return entries;
	}
}
