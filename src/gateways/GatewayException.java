package gateways;

@SuppressWarnings("serial")
public class GatewayException extends Exception {
	
	public GatewayException (Exception e){
		super(e);
	}
}
