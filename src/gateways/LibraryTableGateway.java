package gateways;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import controllers.MasterController;
import models.AuditTrailEntry;
import models.Author;
import models.Book;
import models.Library;
import models.LibraryBook;

public class LibraryTableGateway 
{
	private static final Logger logger = LogManager.getLogger(LibraryTableGateway.class);
	
	// save the connection here
	private Connection conn;

	public LibraryTableGateway() throws GatewayException
	{
		conn = null;
		try{
			Properties props = new Properties();
			FileInputStream stream = new FileInputStream("db.properties");
	        props.load(stream);
	        stream.close();
			
			// Create the data source
			MysqlDataSource ds = new MysqlDataSource();
	        ds.setURL(props.getProperty("MYSQL_DB_URL"));
	        ds.setUser(props.getProperty("MYSQL_DB_USERNAME"));
	        ds.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
	        
	        //create the connection
	        logger.info( "LibraryTableGateway connecting to " + ds.getURL() );
			conn = ds.getConnection();
		}
		catch( IOException | SQLException e )
		{
			e.printStackTrace();
			throw new GatewayException(e);
		}
	}
	/**
	 * Closes the database connection
	 */
	public void close()
	{
		if(conn != null) {
			try {
		        logger.info( "Closing connection" );
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("Unable to close the BookGateway connection");

				//e.printStackTrace();
			}
		}
	}
	
	/**
	 * Returns a list of Libraries where each Library has a list of LibraryBook
	 * objects; then each LibraryBook contains a Book reference;
	 * then each Book has an Author reference.
	 * @return libraries - A List of Libraries.
	 * @throws SQLException
	 */
	public List<Library> getLibraries() throws SQLException
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		List<Library> libraries = new ArrayList<Library>();  // list of Libraries
		Library library = new Library();					 // Library that will 
															 // have a list of LibraryBooks
		List<LibraryBook> libraryBooks = new ArrayList<LibraryBook>(); // list of LibraryBooks
		LibraryBook libraryBook = null; // will instantiate from query
		Book book = null;				// will instantiate from query
		Author author = null;			// will instantiate from query
		int prevLibraryId = 0;
		Library prevLibrary = null; // save history of that last library made

//		SELECT * FROM library as lib 
//		LEFT OUTER JOIN library_book as libBk ON libBk.library_id=lib.id 
//		LEFT OUTER JOIN books as bks ON bks.id=libBk.book_id 
//		LEFT OUTER JOIN authors as a ON a.id=bks.author_id;
		try 
		{
			// get a OUTER JOINed result set of all libraries that may not
			// have library book records
			st = conn.prepareStatement("SELECT * FROM library as lib "
					+ "LEFT OUTER JOIN library_book as libBk ON libBk.library_id=lib.id "
					+ "LEFT OUTER JOIN books as bks ON bks.id=libBk.book_id "
					+ "LEFT OUTER JOIN authors as a ON a.id=bks.author_id");
			rs = st.executeQuery();
			
			while( rs.next() )
			{
				// prepare a new Library object
				library = new Library();
				library.setId(rs.getInt(1));
				library.setLibraryName( rs.getString("library_name"));
				library.setLastModified( rs.getTimestamp(3).toLocalDateTime());

				
				// prepare the library book object
				libraryBook = new LibraryBook();
				
				// do we have a new library? if yes, reset the libraryBook list
				// to get books for this particular library
				if( prevLibraryId != library.getId() )
				{
					//logger.info("Iterating to new LibraryBook List");
					libraryBooks = new ArrayList<LibraryBook>();
				}
				
				// for each library book record 
				if( rs.getInt(4) > 0 ) // check if we pulled library_books for a library
				{
					libraryBook.setNewRecord(false); // set to false, have it from db
					libraryBook.setQuantity(rs.getInt("quantity"));
					libraryBook.setLastModified(rs.getTimestamp(7).toLocalDateTime());
					// set author and book then set book to LibraryBook
					
					// Set the Book object
					book = new Book();
					book.setId( rs.getInt(8));
					book.setTitle(rs.getString("title"));
					book.setPublisher(rs.getString("publisher"));
					book.setDatePublished( rs.getString("date_published"));
					book.setSummary(rs.getString("summary"));
					book.setLastModified( rs.getTimestamp(14).toLocalDateTime());
					
					// set the Author object
					author = new Author( rs.getString("first_name") 
							, rs.getString("last_name"));
					author.setId( rs.getInt(15));
					author.setDateOfBirth( rs.getString("dob"));
					author.setGender( rs.getString("gender"));
					author.setWebsite( rs.getString("web_site"));
					author.setLastModified( rs.getTimestamp(21).toLocalDateTime());

					// set the author into the book
					book.setAuthor( author );
					
					// set book into LibraryBook
					libraryBook.setBook(book);
					
					// add library_book to list of libraryBooks for the library.
					libraryBooks.add(libraryBook);
			
				}// end if existing library_book
				
				if( prevLibrary == null  ) // if we dont have a library yet 
				{
					prevLibrary = library;
					prevLibraryId = library.getId();
					prevLibrary.setLibraryBooks((ArrayList<LibraryBook>) libraryBooks);
				}
				else if( prevLibraryId != library.getId() ) // if have a new library this time
				{
					libraries.add(prevLibrary ); // add prev library to library list
					// add current working libraryBooks to current library
					library.setLibraryBooks((ArrayList<LibraryBook>) libraryBooks);
					prevLibraryId = library.getId();
					prevLibrary = library;
				}
				else // still on same library as last time
				{
					prevLibrary.setLibraryBooks((ArrayList<LibraryBook>) libraryBooks);
				}
			} // end traverse result set
			
			// add the last fetched library
			libraries.add(prevLibrary);
			
		} catch ( SQLException e ) {
			logger.error(" getLibraries() failed to retrieve Library records.");
		} finally {
			if(rs != null )
				rs.close();
			if( st !=null )
				st.close();
		}		
		
		return libraries;
	}
	
	
	@SuppressWarnings("static-access")
	public void insertLibrary(Library library) throws SQLException 
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		int currentSaveId = 0;
		logger.info("Inserting into database");
//		INSERT INTO  `vac921`.`library` (
//				`id` ,
//				`library_name` ,
//				`last_modified`
//				)
//				VALUES (
//				NULL ,  'FBI Secret Library',  '2017-03-28 12:11:00'
//				);

		try {
			st = conn.prepareStatement("INSERT INTO library"
					+ "( id, library_name, last_modified) "
					+ "VALUES ( NULL, ?, CURRENT_TIMESTAMP)" , st.RETURN_GENERATED_KEYS);
			
			conn.setAutoCommit(false); // begin transaction
			
			st.setString(1,  library.getLibraryName());

			st.executeUpdate();

			rs = st.getGeneratedKeys();
			while( rs.next () ) { // save the inserted record's id to avoid duplicate inserts
				
				MasterController.getInstance().setLastSaved( rs.getInt(1) );
				currentSaveId =  MasterController.getInstance().getLastSaved();
				logger.info("Saving Library ID: " + MasterController.getInstance().getLastSaved());
			}
			
			st = conn.prepareStatement("INSERT INTO audit_trail "
					+ "( id ,record_type ,record_id , date_added , entry_msg) "
					+ " VALUES (NULL , 'L',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'L' for Library record_type
			st.setInt( 1, currentSaveId );
			st.setString( 2, "Added");
			
			st.executeUpdate();
			
			conn.commit(); // complete transaction
		
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			logger.error("Failed to insert Library record" );
			conn.rollback();
		}
		finally 
		{
			conn.setAutoCommit( true ); // finally unlock the record
		}	
	} // end insertLibrary()
	
	
	
	public Library getLibraryById(int id) 
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		Library library = new Library();
		List<LibraryBook> libraryBooks = new ArrayList<LibraryBook>();
		Book book = null;
		Author author = null;
		int libraryId = 0;
		try 
		{
			// SELECT * FROM `library` as lib 
			// LEFT OUTER JOIN library_book as libBks 
			// ON lib.id=libBks.library_id WHERE lib.id=1; 
//			st = conn.prepareStatement("SELECT * FROM library as lib "
//					+ "LEFT OUTER JOIN library_book as libBk ON libBk.library_id=lib.id "
//					+ "LEFT OUTER JOIN books as bks ON bks.id=libBk.book_id "
//					+ "LEFT OUTER JOIN authors as a ON a.id=bks.author_id");
			logger.info("Retrieving library with ID = " + id);
			st = conn.prepareStatement("SELECT * FROM library as lib "
					+ "LEFT OUTER JOIN library_book as libBk ON libBk.library_id=lib.id "
					+ "LEFT OUTER JOIN books as bks ON bks.id=libBk.book_id "
					+ "LEFT OUTER JOIN authors as a ON a.id=bks.author_id " 
					+ "WHERE lib.id=?" );
			
			st.setInt(1, id);
			rs = st.executeQuery();
			
			// on each library book record for this library, if any
			while( rs.next() )
			{
				if( libraryId < 1) // if first result set and no library set
				{
					library.setId( rs.getInt(1));
					library.setLibraryName( rs.getString("library_name"));
					library.setLastModified( rs.getTimestamp(3).toLocalDateTime());
					libraryId = library.getId();
				}
				
				// prepare a new library book
				LibraryBook libraryBook = new LibraryBook();
				
				//if the library book exists in the result set for this library
				if( rs.getInt(4) > 0 ) 
				{
					libraryBook.setNewRecord(false); // set to false, have it from db
					libraryBook.setQuantity(rs.getInt("quantity"));
					libraryBook.setLastModified(rs.getTimestamp(7).toLocalDateTime());
					// set author and book then set book to LibraryBook
					
					// Set the Book object
					book = new Book();
					book.setId( rs.getInt(8));
					book.setTitle(rs.getString("title"));
					book.setPublisher(rs.getString("publisher"));
					book.setDatePublished( rs.getString("date_published"));
					book.setSummary(rs.getString("summary"));
					book.setLastModified( rs.getTimestamp(14).toLocalDateTime());
					
					// set the Author object
					author = new Author( rs.getString("first_name") 
							, rs.getString("last_name"));
					author.setId( rs.getInt(15));
					author.setDateOfBirth( rs.getString("dob"));
					author.setGender( rs.getString("gender"));
					author.setWebsite( rs.getString("web_site"));
					author.setLastModified( rs.getTimestamp(21).toLocalDateTime());

					// set the author into the book
					book.setAuthor( author );
					
					// set book into LibraryBook
					libraryBook.setBook(book);
					
					logger.info(String.format("LibraryTable >> Fetched Lib_Book[%s] last_modified: %s"
							, libraryBook.getBook().getId()
							, libraryBook.getLastModified()
							));
					
					// add library_book to list of libraryBooks for the library.
					libraryBooks.add(libraryBook);
				}
				
				// attach the library books to the library
				library.setLibraryBooks( (ArrayList<LibraryBook>) libraryBooks );
				
				//logger.info(String.format("Old Library %s last_modified %s"
				//			, library.getId()
				//			, library.getLastModified()));
			}

		}
		catch(SQLException e)
		{
			logger.error("Failed to fetch Library for ID = " + id);

		}
		return library;
	}
	
	public List<AuditTrailEntry> getLibraryAuditEntries(Library library) throws SQLException
	{
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			st = conn.prepareStatement(
					  "SELECT * FROM library AS l, audit_trail AS at "
					+ "WHERE l.id=at.record_id AND l.id=? AND record_type='L'"
					+ "ORDER BY at.date_added" );
			st.setInt( 1, library.getId() ); // get parameterized Book ID
			logger.info("Fetching audit trail for: " +  library.getLibraryName());
			rs = st.executeQuery();
			while(rs.next() ){
				AuditTrailEntry entry = new AuditTrailEntry();
				logger.info("Fetching date_added ");

				entry.setRecordDescriptor( library.getLibraryName() );
				entry.setDateAdded( rs.getTimestamp( "date_added" ) ); // in table its Timestamp type
				entry.setMessage( rs.getString( "entry_msg"));
				//entry.setRecordDescriptor( "Record ID: " + rs.getInt("record_id" ) 
				//					   + ", Record Type: " + rs.getString("record_type"));
				logger.info("Adding entry with " + entry.getRecordDescriptor());
				entries.add( entry );
			}
		}
		catch ( SQLException e) {	
			logger.error("SQL getLibraryAuditEntries() failed");
			e.printStackTrace();
		}
		return entries;
	}
	public void deleteLibrary(Library library) throws Exception
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		logger.info("Deleting library record from database");
		try {
			conn.setAutoCommit( false ); // begin transaction, lock the record

			st = conn.prepareStatement("DELETE FROM audit_trail WHERE record_id=?");
			st.setInt(1, library.getId() ); // get parameterized author ID
			st.executeUpdate();
			
			st = conn.prepareStatement("DELETE FROM library_book WHERE library_id = ?");
			st.setInt(1,  library.getId() );
			st.executeUpdate();

			st = conn.prepareStatement("DELETE FROM library WHERE id = ?" );
			st.setInt(1, library.getId() ); // get parameterized author ID
			st.executeUpdate();

			conn.commit(); // complete transaction
		}
		catch ( SQLException e ) {
			logger.error("Failed to delete Library record, audit trail, and Library_Book records" );
			conn.rollback();
			//throw new Exception( String.format("All books from %s %s must be deleted first"
			//		, author.getFirstName(), author.getLastName()));
		}
		finally {
			conn.setAutoCommit( true ); // unlock the record
		}
	}
	public void updateLibrary(Library library) throws Exception
	{
		Library oldLibrary = getLibraryById( library.getId() ); // grab the old library record from database
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		ResultSet rs = null;
		PreparedStatement st = null;
		PreparedStatement ast = null;
		logger.info("Updating Library ID " + library.getId() +" into database");
		logger.info(String.format("NEW Library %s last_modified %s"
				, library.getId()
				, library.getLastModified()));
		logger.info(String.format("OLD Library %s last_modified %s"
				, oldLibrary.getId()
				, oldLibrary.getLastModified()));
		
		// check timestamps for optimistic locking
		if( !oldLibrary.getLastModified().equals( library.getLastModified() ))
		{ 
			logger.warn( "last_modified timestamps do not match" );
			//conn.rollback(); // cancel transaction
			throw new Exception( "Someone else has saved before you."
					+ " \nPlease refresh or go back to List and re-select for most recently saved details." );
		}
		

		
		try 
		{
			
			st = conn.prepareStatement("UPDATE library SET "
					+ "library_name = ? "
					//+ "last_modified = CURRENT_TIMESTAMP "
					+ "where id = ?");
			conn.setAutoCommit( false ); // begin transaction
			// if the current first name is different from the one in the database before updating...
			if( !oldLibrary.getLibraryName().equals( library.getLibraryName()))
			{
				// Create an audit trail object
				AuditTrailEntry entry = new AuditTrailEntry();
				entry.setMessage(String.format("Library Name changed from \'%s\' to \'%s\'"
									, oldLibrary.getLibraryName(), library.getLibraryName()) );
				entries.add( entry );
			}
			
			st.setString(1,  library.getLibraryName());
			
			st.setInt( 2, library.getId() );
			
			st.executeUpdate();
			// for each audit message, insert into database
			for( AuditTrailEntry entry : entries )
			{
				ast = conn.prepareStatement("INSERT INTO audit_trail "
						+ "( id ,record_type ,record_id , date_added , entry_msg) "
						+ " VALUES (NULL , 'L',  ?, CURRENT_TIMESTAMP , ? )" );
					ast.setInt( 1, library.getId() );
					ast.setString( 2, entry.getMessage());
					ast.executeUpdate();
			}
			
			conn.commit(); // complete transaction
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			logger.error("Failed to update record" );
		}		
		finally {
			conn.setAutoCommit( true ); // finally unlock the record
		}
	}
	

} // end LibraryTableGateway class
