package gateways;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import controllers.MasterController;
import models.AuditTrailEntry;
import models.Author;
import models.Book;
import models.Library;
import models.LibraryBook;

public class LibraryBookTableGateway 
{
	private static final Logger logger = LogManager.getLogger(LibraryBookTableGateway.class);
	
	// save the connection here
	private Connection conn;

	public LibraryBookTableGateway() throws GatewayException
	{
		conn = null;
		try{
			Properties props = new Properties();
			FileInputStream stream = new FileInputStream("db.properties");
	        props.load(stream);
	        stream.close();
			
			// Create the data source
			MysqlDataSource ds = new MysqlDataSource();
	        ds.setURL(props.getProperty("MYSQL_DB_URL"));
	        ds.setUser(props.getProperty("MYSQL_DB_USERNAME"));
	        ds.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
	        
	        //create the connection
	        logger.info( "LibraryBookTableGateway connecting to " + ds.getURL() );
			conn = ds.getConnection();
		}
		catch( IOException | SQLException e )
		{
			e.printStackTrace();
			throw new GatewayException(e);
		}
	}
	/**
	 * Closes the database connection
	 */
	public void close()
	{
		if(conn != null) {
			try {
		        logger.info( "Closing connection" );
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("Unable to close the BookGateway connection");

				//e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("static-access")
	public void insertLibraryBook(Library library, Book book, int quantity) throws SQLException, Exception
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		PreparedStatement lst = null;
		int currentSaveId = 0;
		logger.info("Inserting library book into database");

		try {
//			INSERT INTO  `vac921`.`library_book` (
//					`library_id` ,
//					`book_id` ,
//					`quantity` ,
//					`last_modified`
//					)
//					VALUES (
//					'1',  '23',  '5',  '2017-03-29 09:15:00'
//					);
			
			st = conn.prepareStatement("INSERT INTO library_book"
					+ "( library_id, book_id, quantity, last_modified) "
					+ "VALUES ( ?, ?, ?, CURRENT_TIMESTAMP)" , st.RETURN_GENERATED_KEYS);
			
			conn.setAutoCommit(false); // begin transaction
			
			st.setInt(1,  library.getId());
			st.setInt(2, book.getId());
			st.setInt(3,  quantity);

			st.executeUpdate();
			
			// update the library entry too
			lst = conn.prepareStatement("UPDATE library SET last_modified = CURRENT_TIMESTAMP where id = ?");
			lst.setInt(1, library.getId());
			lst.executeUpdate();

			rs = st.getGeneratedKeys();
//			while( rs.next () ) { // save the inserted record's id to avoid duplicate inserts
//				
//				MasterController.getInstance().setLastSaved( rs.getInt(1) );
//				currentSaveId =  MasterController.getInstance().getLastSaved();
//				logger.info("Saving Library ID: " + MasterController.getInstance().getLastSaved());
//			}
//			
			st = conn.prepareStatement("INSERT INTO audit_trail "
					+ "( id ,record_type ,record_id , date_added , entry_msg) "
					+ " VALUES (NULL , 'L',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'L' for Library record_type
			st.setInt( 1, library.getId() );
			st.setString( 2, String.format("Added \'%s\' quantity of \'%s\'", quantity, book.getTitle()));
			
			st.executeUpdate();
			
			conn.commit(); // complete transaction
		
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			logger.error("Failed to insert Library_Book record" );
			conn.rollback();

			throw new Exception("The book record \'" + book.getTitle() + "\' has been deleted or is missing. Please refresh or reselect from List to retrieve latest changes for this record.");
		}
		finally 
		{
			conn.setAutoCommit( true ); // finally unlock the record
		}	
	} // end insertLibraryBook()
	

	public LibraryBook getLibraryBookByBookId(int library_id, int book_id) 
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		LibraryBook libraryBook = null;
		Book book = null;
		Author author = null;
		try 
		{
//			SELECT * 
//			FROM library_book AS lb
//			LEFT OUTER JOIN books AS b ON b.id = lb.book_id
//			LEFT OUTER JOIN authors AS a ON b.author_id = a.id
//			WHERE lb.library_id =1
//			AND b.id =21
			logger.info("Retrieving libraryBook with ID = " + book_id);
			st = conn.prepareStatement("SELECT * FROM library_book as lb "
					+ "LEFT OUTER JOIN books as b ON b.id = lb.book_id "
					+ "LEFT OUTER JOIN authors as a ON b.author_id=a.id "
					+ "WHERE lb.library_id=? AND b.id=?");
			
			st.setInt(1, library_id);
			st.setInt(2, book_id);
			rs = st.executeQuery();
			while( rs.next() )
			{
				// prepare a new library book
				libraryBook = new LibraryBook();
				
				//if the library book exists in the result set for this library
				libraryBook.setNewRecord(false); // set to false, have it from db
				libraryBook.setQuantity(rs.getInt(3));
				libraryBook.setLastModified(rs.getTimestamp(4).toLocalDateTime());

				
				// set author and book then set book to LibraryBook
				// Set the Book object
				book = new Book();
				book.setId( rs.getInt(5));
				book.setTitle(rs.getString("title"));
				book.setPublisher(rs.getString("publisher"));
				book.setDatePublished( rs.getString("date_published"));
				book.setSummary(rs.getString("summary"));
				book.setLastModified( rs.getTimestamp(11).toLocalDateTime());
				
				// set the Author object
				author = new Author( rs.getString("first_name") 
						, rs.getString("last_name"));
				author.setId( rs.getInt(12));
				author.setDateOfBirth( rs.getString("dob"));
				author.setGender( rs.getString("gender"));
				author.setWebsite( rs.getString("web_site"));
				author.setLastModified( rs.getTimestamp(18).toLocalDateTime());
	
				// set the author into the book
				book.setAuthor( author );
				
				// set book into LibraryBook
				libraryBook.setBook(book);
				
				logger.info(String.format("Fetched Lib_Book[%s] last_modified: %s"
						, libraryBook.getBook().getId()
						, libraryBook.getLastModified()
						));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			logger.error("Failed to fetch Library for ID = " + library_id);

		}
		return libraryBook;
	}
	
	public void updateLibraryBook(LibraryBook libBook, Library library, Book book, int qty) throws Exception
	{
		logger.info("Updating library book into database");

		LibraryBook oldLibraryBook = getLibraryBookByBookId( library.getId(), book.getId() ); // grab the old library book record from database
		Library oldLibrary = MasterController.getInstance().getLibraryGateway().getLibraryById( library.getId() ); // grab the old libraryrecord from database
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		PreparedStatement st = null;
		PreparedStatement ast = null;
		PreparedStatement lst = null;
		logger.info(String.format("NEW Library Book %s last_modified %s"
				, libBook.getBook().getId()
				, libBook.getLastModified()));
		logger.info(String.format("OLD Library Book %s last_modified %s"
				, oldLibraryBook.getBook().getId()
				, oldLibraryBook.getLastModified()));
		//logger.info("Updating LibraryBook lib_ID " + library.getId() +" into database");
		//logger.info(String.format("New LibraryBook %s last_modified %s"
		//		, libBook.getBook().getId()
		//		, libBook.getLastModified()));
		
		// UPDATE library_book SET quantity = ? WHERE library_id = ? AND book_id = ?
		// check timestamps for optimistic locking
		if( !oldLibraryBook.getLastModified().equals( libBook.getLastModified() ))
		{ 
			logger.warn( "last_modified timestamps do not match" );
			//conn.rollback(); // cancel transaction
			throw new Exception( "Someone else has saved before you."
					+ " \nPlease refresh or go back to List and re-select for most recently saved details." );
		}
		
		try
		{
			conn.setAutoCommit( false ); // begin transaction
			
			if( oldLibraryBook.getQuantity() != qty ) // if diff number of books in a record
			{
				st = conn.prepareStatement("UPDATE library_book SET quantity = ? "
						+ "WHERE library_id = ? AND book_id = ?");
				st.setInt(1, qty);
				st.setInt(2, library.getId());
				st.setInt(3, book.getId());
				st.executeUpdate();
				
				// update the library entry too
				lst = conn.prepareStatement("UPDATE library SET last_modified = CURRENT_TIMESTAMP where id = ?");
				lst.setInt(1, library.getId());
				lst.executeUpdate();

				ast = conn.prepareStatement("INSERT INTO audit_trail "
								+ "( id ,record_type ,record_id , date_added , entry_msg) "
								+ " VALUES (NULL , 'L',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'L' for Library record_type
				ast.setInt( 1, library.getId() );
				ast.setString( 2, String.format(
						"Changed quantity from \'%s\' to \'%s\' for \'%s\'."
						, oldLibraryBook.getQuantity()
						, qty
						, book.getTitle()
						));
				ast.executeUpdate();
			}
			
			conn.commit(); // begin transaction

		}
		catch (SQLException e)
		{
			logger.error("Failed to update Library Record and audit trail" );
			conn.rollback();
			//throw new Exception( String.format("All books from %s %s must be deleted first"
			//		, author.getFirstName(), author.getLastName()));
		}
		finally
		{
			conn.setAutoCommit( true ); // finally unlock the record

		}
		
	}

	
	public void deleteLibraryBooks( int library_id, List<LibraryBook> libBooks ) throws Exception
	{
		ResultSet rs = null;
		PreparedStatement st = null;
		PreparedStatement ast = null;
		PreparedStatement lst = null;
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();

		logger.info(String.format("[%s] Library Book(s) from database", libBooks.size()));
		try {
			conn.setAutoCommit( false ); // begin transaction, lock the record
			for( LibraryBook lb : libBooks )
			{
				// lets check if we're allowed to delete based on timestamps
				LibraryBook oldLibraryBook = getLibraryBookByBookId( library_id, lb.getBook().getId()); // grab the old library book record from database
				logger.info(String.format("NEW Library Book %s last_modified %s"
						, lb.getBook().getId()
						, lb.getLastModified()));
				logger.info(String.format("OLD Library Book %s last_modified %s"
						, oldLibraryBook.getBook().getId()
						, oldLibraryBook.getLastModified()));
				if( !oldLibraryBook.getLastModified().equals( lb.getLastModified() ))
				{ 
					logger.warn( "last_modified timestamps do not match" );
					conn.rollback(); // cancel transaction
					throw new Exception( "Someone else has saved before you."
							+ " \nPlease refresh or go back to List and re-select for most recently saved details." );
				}
				
				st = conn.prepareStatement("DELETE FROM library_book WHERE library_id = ? AND book_id = ?");
				st.setInt(1,  library_id);
				st.setInt(2,  lb.getBook().getId());
				st.executeUpdate();
				
				ast = conn.prepareStatement("INSERT INTO audit_trail "
						+ "( id ,record_type ,record_id , date_added , entry_msg) "
						+ " VALUES (NULL , 'L',  ?, CURRENT_TIMESTAMP , ? )" ); // using 'L' for Library record_type
				ast.setInt( 1, library_id );
				ast.setString( 2, String.format("Removed \'%s\' ", lb.getBook().getTitle()));
				ast.executeUpdate();
				
				// update the library entry too
				lst = conn.prepareStatement("UPDATE library SET last_modified = CURRENT_TIMESTAMP where id = ?");
				lst.setInt(1, library_id);
				lst.executeUpdate();
			}

			conn.commit(); // complete transaction
		}
		catch ( SQLException e ) {
			logger.error("Failed to delete Library_Book records" );
			conn.rollback();
			//throw new Exception( String.format("All books from %s %s must be deleted first"
			//		, author.getFirstName(), author.getLastName()));
		}
		finally {
			conn.setAutoCommit( true ); // unlock the record
		}
	}

} // end LibraryBookTableGateway class
