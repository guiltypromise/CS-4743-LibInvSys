/**
 * Audit Trail Entry Model
 * @author Jason Martin
 * 
 * Description:
 * 		Business rules for an Audit Trail entry into the database.
 * 
 * Notes:
 * 		
 */
package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AuditTrailEntry {

	/**
	 * A descriptor for the record
	 */
	private String recordDescriptor;
	
	/**
	 * The date this audit trail entry was added
	 */
	private Date dateAdded;
	
	/**
	 * The message this audit trail entry will contain.
	 */
	private String message;
	
	/**
	 * Creates an empty AuditTrailEntry template.
	 */
	public AuditTrailEntry() 
	{
		this.recordDescriptor = "";
		this.dateAdded = null;
		this.message = "";
	}
	
	/**
	 * Constructs an AuditTrailEntry with a required Tiemstamp argument.
	 * @param date The date and time this audit trial was entered.
	 */
	public AuditTrailEntry(Date date) 
	{
		this.recordDescriptor = "";
		this.dateAdded = date;
		this.message = "";
	}

	public AuditTrailEntry(String first, String last) {
		this.recordDescriptor = first + " " + last;
		this.dateAdded = null;
		this.message = "";
	}

	public String getRecordDescriptor() {
		return recordDescriptor;
	}

	public void setRecordDescriptor(String recordDescriptor) {
		this.recordDescriptor = recordDescriptor;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date date) {
		this.dateAdded = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * A string representation of this AuditTrailEntry in the following form:
	 * <p>
	 * "yyyy-mm-dd hh:mm:ss� "Message"
	 * </p>
	 * 
	 * @return output A String representation of this AuditTrailEntry.
	 */
	@Override
	public String toString()
	{
		String output = "";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// format timestamp
		String timestamp = df.format( dateAdded );
		
		// append message
		output += timestamp + " " + this.getMessage() ;
		
		return output;
	}
}
