/**
 * LibraryBook.java 
 * @author Jason Martin
 * 
 * Notes:
 * 	This model is an association class that will house a single record from the
 *  library_book junction table and provides the domain model representation
 *  of the relationship between a library and its books, and how many
 *  copies of each book the library has in its inventory.
 */

package models;

import java.sql.SQLException;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controllers.MasterController;
import gateways.LibraryBookTableGateway;

public class LibraryBook 
{
	/*--------------  Attributes ---------------------------*/
	private static final Logger logger = LogManager.getLogger(LibraryBook.class);

	private Book book;
	
	private int quantity;
	
	private boolean newRecord; // boolean, defaults to True, 
							   // set to False when instantiated using data
							   // from a database record
	private LocalDateTime lastModified; // timestamp of last saved edit
	
	/*--------------  Constructors ---------------------------*/
	/**
	 * Constructs a Library Book model
	 */
	public LibraryBook( Book book ) 
	{
		this.book = book;
		quantity = 1;
		newRecord = true;
		lastModified = null;
	}
	
	public LibraryBook()
	{
		book = null;
		quantity = 0;
		newRecord = true;
		lastModified = null;
	}
	
	/*--------------  Methods ---------------------------*/

	
	@Override
	public String toString() 
	{
		return "Qty: " + quantity + "\tTitle: " + book ;
	}

	public Book getBook() 
	{
		return book;
	}

	public void setBook(Book book) 
	{
		this.book = book;
	}

	public int getQuantity() 
	{
		return quantity;
	}

	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}

	public boolean isNewRecord() 
	{
		return newRecord;
	}

	public void setNewRecord(boolean newRecord) 
	{
		this.newRecord = newRecord;
	}
	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * Saves this new library book record into the database
	 * @param isNew		Helper variable to determine if its new or it exists in db
	 * @param library	The library to associate this library book record
	 * @param book  The selected book
	 * @param qty   The specified quantity
	 */
	public void saveLibraryBook(boolean isNew, Library library, Book book, int qty ) throws Exception
	{
		logger.info("Saving: Library Book record");

		try {
			if( isNew )
			{
				logger.info("Saving new Library Book record");
				MasterController.getInstance().getLibraryBookTableGateway().insertLibraryBook( library, book, qty);
			}
			else
			{
				logger.info("Updating Library Book record");
				MasterController.getInstance().getLibraryBookTableGateway().updateLibraryBook(this,library, book, qty);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new Exception ( e.getMessage());
		}
	}

}
