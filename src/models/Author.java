package models;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controllers.MasterController;
import core.Launcher;

public class Author 
{
	// Author attributes
	private int id;					// unique id
	private String firstName;   	// first name
	private String lastName;		// last name
	private String gender;			// gender
	private String dob;				// date of birth string
	private String webSite;			// author's website
	private LocalDateTime lastModified; // the last modified timestamp of an author
	private static final Logger logger = LogManager.getLogger(Launcher.class);

	
	public Author() 
	{
		this.id = 0;
		this.firstName = "";
		this.lastName = "";
		//this.gender = "M";
		this.gender = "";
		//this.dob = "1970-01-01";
		this.dob = "";
		this.webSite = "";
		this.lastModified = null;
	}
	
	/**
	 * Constructs a new Author with a given first name and last name.
	 * @param firstName The first name of this author.
	 * @param lastName The last name of this author.
	 */
	public Author(String firstName, String lastName )
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public void setId( int id )
	{
		this.id = id;
	}
	
	public int getId()
	{
		return this.id;
	}
	
	/*     First and Last name setters/getters      */
	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}
	
	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}
	
	public String getFirstName()
	{
		return this.firstName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	/*         Date of birth stuff    */
	
	public LocalDate getDateOfBirth()
	{
		// convert date string to LocalDate
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
		
		//convert String to LocalDate
		if( !validateDob( dob.toString() ) )
		{
			return null;
		}
		LocalDate localDate = LocalDate.parse(dob, formatter);
		return localDate;
		//return this.dob;
	}
	
	public void setDateOfBirth( String date )
	{
		this.dob = date;
	}
	
	/* 			Gender stuff  		*/
	public String getGender()
	{
		return this.gender;
	}
	
	public void setGender( String gender )
	{
		this.gender = gender;
	}
	
	
	/*			Website stuff			*/
	public String getWebsite()
	{
		return this.webSite;
	}
	
	public void setWebsite( String website )
	{
		this.webSite = website;
	}
	
	/**
	 * Validates and saves the desired input for an author into the database.
	 * @param id The unique id of the author.
	 * @param first The first name of the author.
	 * @param last The last name of the author.
	 * @param gender The gender of the author
	 * @param web The author's website.
	 * @throws Exception 
	 */
	public void saveAuthor( int id, String first, String last, String dob, String gender, String web ) throws Exception
	{
		// validate all input parms
		if( !validateId( id ) )
		{
			// give some error
			throw new Exception("Invalid id");
		}
		if( !validateFirstName( first ) || first.equals(null))
		{
			throw new Exception("First name is empty or greater than 100 chars");
		}
		if( !validateLastName( last ))
		{
			throw new Exception("Last name is empty or greater than 100 chars");
		}
		if( !validateDob( dob ) || dob == null )
		{
			throw new Exception("Date of birth invalid format (mm/dd/yyyy) or not selected");
		}
		if( !validateGender( gender ) || gender == null)
		{
			throw new Exception("Selected gender is not Male, Female, or Unknown");
		}
		if( !validateWebsite( web ) )
		{
			throw new Exception("Website is greater than 100 chars in length");
		}

		
		// call gateway update
		try {
			this.setDateOfBirth( dob );
			this.setFirstName(first);
			this.setLastName(last);
			this.setGender(gender);
			this.setWebsite(web);
			
			if( this.getId() < 1 ) // if author is new, insert
				MasterController.getInstance().getAuthorGateway().insertAuthor( this );
			else // we can update the existing author's info
				MasterController.getInstance().getAuthorGateway().updateAuthor( this );
			
		} catch(SQLException e ) {
			if( this.getId() < 1 )
				logger.error("Failed to insert author data");
			else
				logger.error("Failed to update author data");
			//e.printStackTrace();
		}
	}

	
	/**
	 * Returns a string representation of this Author with an id, first name, and last name.
	 * @return A string representation of this Author.
	 */
	@Override
	public String toString() {
		return "[" + id  + "]\t" + firstName + " " + lastName;
	}
	
	/**
	 * Checks the associated unique id if its >= 0
	 * @param id The unique ID to be validated
	 * @return TRUE if the ID passes validation, FALSE otherwise.
	 */
	public boolean validateId( int id )
	{
		if( id >= 0 )
			return true;
		else 
			return false;
	}
	
	/**
	 * Checks the desired input for a first name that is not empty and is less than 100 chars in length.
	 * @param firstName The string input for this Author's first name.
	 * @return TRUE if the input is not empty and is less than 100 chars in length, FALSE otherwise.
	 */
	public boolean validateFirstName( String firstName )
	{
		if( !firstName.isEmpty() ) // check if empty string input
		{
			if( firstName.length() < 100 ) // check if input too long 
			{
				return true;
			}
		}
		// fall through invalidation and return false
		return false;
	}
	
	/**
	 * Checks the desired input for a last name that is not empty and is less than 100 chars in length.
	 * @param lastName The string input for this Author's last name.
	 * @return TRUE if the input is not empty and is less than 100 chars in length, FALSE otherwise.
	 */
	public boolean validateLastName( String lastName )
	{
		if( !lastName.isEmpty() ) // check if empty string input
		{
			if( lastName.length() < 100 ) // check if input too long 
			{
				return true;
			}
		}
		// fall through invalidation and return false
		return false;
	}
	
	public boolean validateDob( String date )
	{
		//if( !date.isEmpty() )
		//	return true;
		// try parsing the date
		// convert date string to LocalDate
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
			//convert String to LocalDate
			@SuppressWarnings("unused")
			LocalDate localDate = LocalDate.parse(dob, formatter);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	/**
	 * Checks the desired input for gender for Male, Female, or Unknown.
	 * @param genderInput The desired input for gender.
	 * @return TRUE if the gender is Male, Female, or Unknown. FALSE otherwise.
	 */
	public boolean validateGender( String genderInput )
	{
		if( genderInput == null )
			return false;
		String gender = genderInput.toLowerCase();
		if( gender.equals("m") || gender.equals("f") || gender.equals("unknown") )
			return true;
		if( gender.equals("male") || gender.equals("female") || gender.equals("unknown") )
			return true;
		// fall through invalidation
		return false;
	}
	
	/**
	 * Checks the desired input for a website if its less than 100 chars in length.
	 * @param website The desired input for a website
	 * @return TRUE if the website is less than 100 chars, FALSE otherwise.
	 */
	public boolean validateWebsite ( String website )
	{
		if( website.length() < 100 )
			return true;
		// fall through invalidation
		return false;
	}

	public List<AuditTrailEntry> getAuditTrail() {
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		try {
			entries = MasterController.getInstance().getAuthorGateway().getAuthorAuditEntries(this);
		} catch (SQLException e) {
			logger.error("Failed to fetch audit trail for " + this.firstName + " " + this.lastName);
			//e.printStackTrace();
		}
		return entries;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}
}
