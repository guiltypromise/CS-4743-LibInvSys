package models;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controllers.MasterController;

public class Library 
{
	/*--------------  Attributes ---------------------------*/
	private static final Logger logger = LogManager.getLogger(Library.class);

	private int id;
	
	private String libraryName;
	
	private ArrayList<LibraryBook> libraryBooks;
	
	private LocalDateTime lastModified; // timestamp of last saved edit

	
	/*--------------  Constructors ---------------------------*/
	/**
	 * Constructs a Library Book model
	 */
	public Library() 
	{
		this.id = 0;
		this.libraryName = "";
		this.libraryBooks = new ArrayList<LibraryBook>();
		this.lastModified = null;
	}

	/*--------------  Methods ---------------------------*/


	@Override
	public String toString() {
		return "[" + id + "]\t" + " " + libraryName;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLibraryName() {
		return libraryName;
	}


	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}


	public ArrayList<LibraryBook> getLibraryBooks() {
		return libraryBooks;
	}


	public void setLibraryBooks(ArrayList<LibraryBook> books) {
		this.libraryBooks = books;
	}


	public LocalDateTime getLastModified() {
		return lastModified;
	}


	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}

	public List<AuditTrailEntry> getAuditTrail() throws Exception
	{
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		try {
			entries = MasterController.getInstance().getLibraryGateway().getLibraryAuditEntries(this);
		} catch (SQLException e) {
			logger.error("Failed to fetch audit trail for " + this.getLibraryName());
			//e.printStackTrace();
		}
		return entries;
	}

	/**
	 * Validates and saves the desired input for a Library into the database.
	 * @param id
	 * @param name
	 * @param libraryBooks
	 */
	public void saveLibrary( int id, String name, List<LibraryBook> libraryBooks) throws Exception
	{
		if( !validateId( id ) )
		{
			// give some error
			throw new Exception("Invalid ID");
		}
		if( !validateName( name ) || name.equals(null))
		{
			throw new Exception("\'Library Name\' is empty or greater than 100 chars");
		}
		if( !validateLibraryBookQty(libraryBooks) )
		{
			throw new Exception("Each library book must have a quantity >= 0");
		}
		if( !validateLibBookList( libraryBooks) )
		{
			throw new Exception("Each library book must have a valid book entry and cannot be duplicated in inventory");
		}
		
		try {
			// set the Library model before passing to Gateway
			this.setLibraryName(name);
			//this.setLibraryBooks((ArrayList<LibraryBook>) libraryBooks);
			
			
			if( this.getId() < 1 )
			{
				logger.info("Saving new Library to database");	
				MasterController.getInstance().getLibraryGateway().insertLibrary( this );
				int lib_id = MasterController.getInstance().getLastSaved();
				Library updatedLibrary = MasterController.getInstance().getLibraryGateway().getLibraryById(lib_id);

				for( LibraryBook libBook : libraryBooks )
				{
					// possibly create a new record or update the quantity of an existing book entry
					MasterController.getInstance().getLibraryBookTableGateway().insertLibraryBook(updatedLibrary
							, libBook.getBook()
							, libBook.getQuantity());
					//libBook.saveLibraryBook(libBook.isNewRecord(), updatedLibrary, libBook.getBook(), libBook.getQuantity());
				}
			}
			else
			{
				logger.info("Updating this Library to database");	

				MasterController.getInstance().getLibraryGateway().updateLibrary( this );
				// now create or update library records
				for( LibraryBook libBook : libraryBooks )
				{
					//libBook.setLastModified(this.getLastModified());
					logger.info(String.format("Updating Lib_Book[%s] last_modified: %s"
							, libBook.getBook().getId()
							, libBook.getLastModified()
							));
					if( libBook.isNewRecord() )
					{
						MasterController.getInstance().getLibraryBookTableGateway().insertLibraryBook(this
								, libBook.getBook()
								, libBook.getQuantity());
					}
					else
					{
						//libBook.saveLibraryBook(libBook.isNewRecord(), this, libBook.getBook(), libBook.getQuantity());
						MasterController.getInstance().getLibraryBookTableGateway().updateLibraryBook(libBook
								, this
								, libBook.getBook()
								, libBook.getQuantity());
					}

				}
			}

		} catch (Exception e ) {
			if( this.getId() < 1 )
				logger.error("Failed to INSERT Library data");
			else
				logger.error("Failed to UPDATE Library data");
			//e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		
	}
	
	/* -------------- Validation Rules -------------------- */ 

	private boolean validateId( int id )
	{
		if( id >= 0 )
			return true;
		return false;
	}

	private boolean validateName( String name )
	{
		if( !name.isEmpty() )
		{
			if( name.length() < 100 )
				return true;
		}
		return false;
	}

	
	//each LibraryBook object in a Library�s list of LibraryBooks must have a valid Book
	//that is not already in its list of LibraryBooks
	private boolean validateLibraryBookQty( List<LibraryBook> lb )
	{
		for( LibraryBook libBook : lb )
		{
			if( libBook.getQuantity() < 0 )
				return false;
		}
		return true;
	}
	
	// library_book has book_id
	// each LibraryBook object in a Library�s list of LibraryBooks 
	// must have a valid Book that is not already in its list of LibraryBooks
	// BASICALLY means the Book better not be duplicated in the list
	private boolean validateLibBookList( List<LibraryBook> lb ) 
	{
		ArrayList<Book> bookHistory = new ArrayList<Book>();
		// for each library_book record
		for( LibraryBook libBook : lb )
		{
			// if the LibraryBook Book is invalid
			if( !libBook.getBook().validateSelf() )
			{
				return false;
			}
			
			// check if in the history list
			if( bookHistory.contains(libBook.getBook()))
			{
				// if found in the list, not a valid LibraryBook list
				return false;
			}
			
			// add valid book to history list to keep track for duplicates
			bookHistory.add( libBook.getBook() );
		}
		
		bookHistory.clear(); // optional history cleanup
		return true;
	}
}
