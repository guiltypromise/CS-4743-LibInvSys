package models;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controllers.MasterController;

public class Book {
	
	private int id;					// a book's unique id
	private String title;			// title of the book
	private String publisher;		// name of the publisher
	private String datePublished;	// date published in yyyy-mm-dd
	private String summary;			// summary of the book plot
	private Author author;			// the author of the book
	private LocalDateTime lastModified; // timestamp of last saved edit
	
	private static final Logger logger = LogManager.getLogger(Book.class);

	/**
	 * Constructs an empty Book object.
	 */
	public Book() 
	{
		this.id = 0;
		this.title = "";
		this.publisher = "";
		this.datePublished = "";
		this.summary = "";
		this.author = null;
		this.lastModified = null;
	}

	public Book(String title) {
		this.title = title;
	}

	/**
	 * A string representation of this 
	 */
	@Override
	public String toString() 
	{
		return String.format( "[%s]\t %s by %s %s"
				, this.id
				, this.title
				, author.getFirstName()
				, author.getLastName());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public LocalDate getDatePublished() 
	{
		// convert date string to LocalDate
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
		
		//convert String to LocalDate
		if( !validateDatePublished( datePublished.toString() ) )
		{
			return null;
		}
		LocalDate localDate = LocalDate.parse(datePublished, formatter);
		return localDate;
	}



	public void setDatePublished(String datePublished) 
	{
		this.datePublished = datePublished;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}
	
	/**
	 * Validates and saves the desired input for a Book into the database.
	 * @param id
	 * @param title
	 * @param publisher
	 * @param datePublished
	 * @param author
	 * @param summary
	 */
	public void saveBook(int id
			, String title
			, String publisher
			, String datePublished
			, Author author,
			String summary) throws Exception
	{
		// validate all input parms
		if( !validateId( id ) )
		{
			// give some error
			throw new Exception("Invalid ID");
		}
		if( !validateTitle( title ) || title.equals(null))
		{
			throw new Exception("\'Title\' is empty or greater than 100 chars");
		}
		if( !validatePublisher( publisher ) || publisher.equals(null))
		{
			throw new Exception("\'Publisher\' is empty or greater than 100 chars");
		}
		if( !validateDatePublished( datePublished ) || datePublished == null )
		{
			throw new Exception("Date published invalid format (mm/dd/yyyy) or not selected");
		}
		if( !validateAuthor( author ) || author == null)
		{
			throw new Exception("Book needs an associated Author");
		}
		if( !validateSummary( summary ) )
		{
			throw new Exception("Book summary is empty");
		}
		
		try {
			// set the Book model before passing to Gateway
			this.setTitle( title );
			this.setPublisher(publisher);
			this.setDatePublished(datePublished);
			this.setAuthor(author);
			this.setSummary(summary);
			
			
			if( this.getId() < 1 )
			{
				logger.info("Saving new Book to database");	
				MasterController.getInstance().getBookGateway().insertBook( this );
			}
			else
			{
				logger.info("Updating this Book to database");	
				MasterController.getInstance().getBookGateway().updateBook( this );
			}
		} catch( SQLException e ) {
			if( this.getId() < 1 )
				logger.error("Failed to insert author data");
			else
				logger.error("Failed to update author data");
			//e.printStackTrace();
		}
	}
	
	public List<AuditTrailEntry> getAuditTrail() throws Exception
	{
		List<AuditTrailEntry> entries = new ArrayList<AuditTrailEntry>();
		try {
			entries = MasterController.getInstance().getBookGateway().getBookAuditEntries(this);
		} catch (SQLException e) {
			logger.error("Failed to fetch audit trail for " + this.getTitle());
			//e.printStackTrace();
		}
		return entries;
	}

	/* -------------- Validation Rules -------------------- */ 
	
	public boolean validateSelf() 
	{
		// validate all input parms
		if( !validateId( id ) )
		{
			// give some error
			//throw new Exception("Invalid ID");
			return false;
		}
		if( !validateTitle( title ) || title.equals(null))
		{
			//throw new Exception("\'Title\' is empty or greater than 100 chars");
			return false;
		}
		if( !validatePublisher( publisher ) || publisher.equals(null))
		{
			//throw new Exception("\'Publisher\' is empty or greater than 100 chars");
			return false;
		}
		if( !validateDatePublished( datePublished ) || datePublished == null )
		{
			return false;
			//throw new Exception("Date published invalid format (mm/dd/yyyy) or not selected");
		}
		try {
			if( !validateAuthor( author ) || author == null)
			{
				return false;
				//throw new Exception("Book needs an associated Author");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		if( !validateSummary( summary ) )
		{
			return false;
			//throw new Exception("Book summary is empty");
		}
		return true;
	}
	
	// validate this book's ID
	private boolean validateId( int id )
	{
		if( id >= 0 ) // when 0, we will make a new Book
			return true;
		return false;
	}
	
	// validate title
	private boolean validateTitle( String title )
	{
		if( !title.isEmpty() ) // check if empty string input
		{
			if( title.length() < 100 ) // check if input too long 
			{
				return true;
			}
		}
		// fall through invalidation and return false
		return false;
	}
	
	// validate publisher
	private boolean validatePublisher( String publisher )
	{
		if( !publisher.isEmpty() ) // check if empty string input
		{
			if( publisher.length() < 100 ) // check if input too long 
			{
				return true;
			}
		}
		// fall through invalidation and return false
		return false;
	}
	
	// validate summary
	private boolean validateSummary( String summary )
	{
		if( !summary.isEmpty() ) // check if empty string input
		{
			return true;
		}
		// fall through invalidation and return false
		return false;
	}
	
	
	// validate date published
	private boolean validateDatePublished(String string) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
			//convert String to LocalDate
			@SuppressWarnings("unused")
			LocalDate localDate = LocalDate.parse(datePublished, formatter);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	private boolean validateAuthor( Author author ) throws Exception
	{
		// see if its an existing author
		if( !(author instanceof Author) )
			throw new Exception("Book needs an associated Author");
		if( author.getId() > 0 )
			return true;
	
		return false;
	}

}
